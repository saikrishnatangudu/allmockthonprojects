package com.flight.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.flight.dto.BookingDto;
import com.flight.dto.FlightDto;
import com.flight.exception.BookingNotfoundException;
import com.flight.exception.DataNotFound;
import com.flight.exception.FlightNotFoundException;
import com.flight.exception.SeatsNotAvilableException;
import com.flight.exception.TransactionFailedException;
import com.flight.exception.UserNotfoundException;
import com.flight.model.Booking;
import com.flight.model.Flight;
import com.flight.model.User;
import com.flight.repository.BookingRepo;
import com.flight.repository.FlightRepo;
import com.flight.repository.UserRepo;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BookingServiceTest {
	@InjectMocks
	FlightBookingServiceImpl flightBookingService;
	@Mock
	BookingRepo bookingRepo;
	@Mock
	RestTemplate restTemplate;
	@Mock
	FlightRepo flightRepo;
	@Mock
	UserRepo userRepo;

	@Test(expected = UserNotfoundException.class)
	public void bookBusServicefrusr() {
		BookingDto bookingDto = new BookingDto();
		Booking booking = new Booking();
		Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(null);
		flightBookingService.makeBooking(bookingDto);
	}

	@Test(expected = FlightNotFoundException.class)
	public void bookBusServiceTestbus() {
		BookingDto bookingDto = new BookingDto();
		Booking booking = new Booking();
		User usr = new User();
		usr.setEmail("sk");
		Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(usr);
		Assert.assertNotNull(usr);
		Assert.assertEquals(booking, flightBookingService.makeBooking(bookingDto));

	}

	/*
	 * @Test(expected = NullPointerException.class) public void bookBusServicef() {
	 * BookingDto bookingDto = new BookingDto();
	 * 
	 * bookingDto.setNoOfSeatsBooked(2); bookingDto.setFlightId(1); Booking booking
	 * = new Booking(); User usr = new User(); BeanUtils.copyProperties(bookingDto,
	 * booking); Flight b = new Flight(); b.setFare(100.0); Optional<Flight> bus =
	 * Optional.ofNullable(new Flight());
	 * 
	 * 
	 * usr.setEmail("sk");
	 * Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(usr);
	 * booking.setAmount(f1.getFare() * fli.getNoOfSeatsBooked());
	 * Mockito.when(flightRepo.findById(1)).thenReturn(bus); flightRepo.save(b);
	 * 
	 * booking.setUser(usr); booking.setFlight(b);
	 * 
	 * Assert.assertNotNull(usr); Assert.assertEquals(booking,
	 * flightBookingService.makeBooking(bookingDto));
	 * 
	 * }
	 */

	@Test(expected = FlightNotFoundException.class)
	public void bookBusServicefkk() {
		
		BookingDto bookingDto = new BookingDto();
		Booking booking = new Booking(11, new Date(2020 - 04 - 15), 3, 9000.00, new Date(2020 - 04 - 20));

		User usr = new User();
		usr.setEmail("sk");
		Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(usr);
		Flight b = new Flight();
		b.setFlightId(1);
		b.setFare(100.0);
		if(b.getNoOfSeatsAvailable() > bookingDto.getNoOfSeatsBooked()) {
			
		}
			
			
			
		Mockito.when(flightRepo.findById(b.getFlightId())).thenReturn(null);	
		booking.setUser(usr);
		booking.setFlight(b);
		Assert.assertNotNull(usr);
		Assert.assertEquals(booking, flightBookingService.makeBooking(bookingDto));
	}
	
	//need to be changed
	/*
	 * @Test public void bookBusServicefk() {
	 * 
	 * BookingDto bookingDto = new BookingDto(); Booking booking = new Booking(11,
	 * new Date(2020 - 04 - 15), 3, 9000.00, new Date(2020 - 04 - 20));
	 * 
	 * User usr = new User(); usr.setEmail("sk");
	 * Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(usr);
	 * List<Flight> flights = new ArrayList<Flight>(); Flight flight = new Flight();
	 * FlightDto flightDto = new FlightDto(); flight.setFlightId(1);
	 * flight.setDestination("bangalore"); flight.setSource("salem");
	 * flights.add(flight); Optional<OngoingStubbing<Optional<Flight>>>
	 * option=Optional.of(Mockito.when(flightRepo.findById(1)).thenReturn(Optional.
	 * of(flight))); OngoingStubbing<Optional<Flight>> flight1=null; if
	 * (option.isPresent()) { System.out.println(option.isPresent()); flight1 =
	 * option.get(); System.out.println(flight1); }
	 * 
	 * Assert.assertEquals(booking, flightBookingService.makeBooking(bookingDto)); }
	 */

	public BookingServiceTest() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Test
	public void booResponseEntity() {
		BookingDto bookingDto = new BookingDto();

		Assert.assertEquals(null, flightBookingService.transction(bookingDto, 2));

	}

	@Test
	public void booResponseEntitypos() {
		BookingDto bookingDto = new BookingDto();

		String uri = "http://BANKING-SERVICE/transaction";
		double sum = 5;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("accountNumber", bookingDto.getAccountNumber());
		request.put("amount", sum);
		request.put("banificiaryAccountNo", 6666);

		HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);
		Assert.assertEquals(null, flightBookingService.transction(bookingDto, 5));

	}

	@Test
	public void testFindByIdForNegative() {
		Booking booking = new Booking(-1, new Date(2020 - 04 - 15), 2, 3000.00, new Date(2020 - 04 - 20));
		Mockito.when(bookingRepo.findById(-1)).thenReturn(Optional.of(booking));
		Booking book = flightBookingService.getBookingById(-1);
		Assert.assertNotNull(book);
		Assert.assertEquals(2, book.getNoOfSeatsBooked());
	}

	@Test
	public void testFindByIdForPositive() {
		Booking booking = new Booking(11, new Date(2020 - 04 - 15), 3, 9000.00, new Date(2020 - 04 - 20));
		Mockito.when(bookingRepo.findById(11)).thenReturn(Optional.of(booking));
		Booking book = flightBookingService.getBookingById(11);
		Assert.assertNotNull(book);
		Assert.assertEquals(3, book.getNoOfSeatsBooked());
	}

	@Test
	public void testFindByIdForPositiveDate() {
		Booking booking = new Booking(11, new Date(2020 - 04 - 15), 3, 9000.00, new Date(2020 - 04 - 20));
		Mockito.when(bookingRepo.findById(11)).thenReturn(Optional.of(booking));
		Booking book = flightBookingService.getBookingById(11);
		Assert.assertNotNull(book);
		Assert.assertEquals(new Date(2020 - 04 - 15), book.getBookingDate());
	}

	@Test(expected = BookingNotfoundException.class)
	public void testFindByIdForException() throws BookingNotfoundException {
		Booking booking = new Booking(11, new Date(2020 - 04 - 15), 3, 9000.00, new Date(2020 - 04 - 20));
		Mockito.when(bookingRepo.findById(11)).thenReturn(Optional.of(booking));
		Booking book = flightBookingService.getBookingById(14);
		Assert.assertNotNull(book);
		Assert.assertEquals(4, book.getNoOfSeatsBooked());
	}

}
