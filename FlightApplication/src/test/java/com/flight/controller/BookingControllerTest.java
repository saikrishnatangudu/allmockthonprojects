package com.flight.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.flight.dto.BookingDto;
import com.flight.model.Booking;
import com.flight.service.FlightBookingService;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BookingControllerTest {
	
	
	@InjectMocks
	FlightBookingController flightBookingController;
	
	@Mock
	FlightBookingService flightBookingService;
	@Test
	public void testBusbooking() {
		BookingDto bookingDto =new BookingDto();
		Booking booking =new Booking();
		bookingDto.setFlightId(1);
		bookingDto.setEmailId("sai@gmail.com");
		Mockito.when(flightBookingService.makeBooking(bookingDto)).thenReturn(booking);
		Assert.assertEquals(booking, flightBookingService.makeBooking(bookingDto));
		
	}
	
	@Test
	public void testBusbookingpos() {
		BookingDto bookingDto =new BookingDto();
		Booking booking =new Booking();
		bookingDto.setFlightId(1);
		bookingDto.setEmailId("sai@gmail.com");
		Mockito.when(flightBookingService.makeBooking(bookingDto)).thenReturn(booking);
		Assert.assertNotNull(booking);
		ResponseEntity<Booking> b=flightBookingController.makeBooking(bookingDto);
	
		
		Assert.assertEquals(HttpStatus.OK, b.getStatusCode());
		
	}
	
	@Test
	public void testGetbooking() {
		BookingDto bookingDto =new BookingDto();
		Booking booking =new Booking();
		bookingDto.setFlightId(1);
		bookingDto.setEmailId("sai@gmail.com");
		Mockito.when(flightBookingService.makeBooking(bookingDto)).thenReturn(booking);
		Assert.assertEquals(booking, flightBookingService.makeBooking(bookingDto));
		
	}
	@Test
	public void testStudentByIdForPositive() {
		Booking booking = new Booking(1, new Date(2020 - 04 - 15), 2, 3000.00, new Date(2020 - 04 - 20));
		Mockito.when(flightBookingService.getBookingById(1)).thenReturn(booking);
		ResponseEntity<Booking> book = flightBookingController.getBookingById(1);
		Assert.assertNotNull(book);
		Assert.assertEquals(HttpStatus.OK, book.getStatusCode());

	}

	@Test
	public void testStudentByIdForNegative() {
		Booking booking = new Booking(-1, new Date(2020 - 04 - 15), 2, 3000.00, new Date(2020 - 04 - 20));
		Mockito.when(flightBookingService.getBookingById(-1)).thenReturn(booking);
		ResponseEntity<Booking> book = flightBookingController.getBookingById(-1);
		Assert.assertNotNull(book);
		Assert.assertEquals(HttpStatus.OK, book.getStatusCode());

	}
	
	
}
