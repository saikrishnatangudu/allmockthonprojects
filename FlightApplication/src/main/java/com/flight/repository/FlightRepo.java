package com.flight.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flight.model.Flight;

public interface FlightRepo extends JpaRepository<Flight, Integer>{
	List<Flight> findFlightBySourceAndDestinationAndJourneyDate(String source, String destination, Date journeyDate);
}
