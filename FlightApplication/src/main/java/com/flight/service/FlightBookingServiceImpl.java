package com.flight.service;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.flight.dto.BookingDto;
import com.flight.exception.BookingNotfoundException;
import com.flight.exception.FlightNotFoundException;
import com.flight.exception.SeatsNotAvilableException;
import com.flight.exception.TransactionFailedException;
import com.flight.exception.UserNotfoundException;
import com.flight.model.Booking;
import com.flight.model.Flight;
import com.flight.model.User;
import com.flight.repository.BookingRepo;
import com.flight.repository.FlightRepo;
import com.flight.repository.UserRepo;

@Service
public class FlightBookingServiceImpl implements FlightBookingService {

	@Autowired
	UserRepo userRepo;
	@Autowired
	FlightRepo flightRepo;
	@Autowired
	BookingRepo bookingRepo;
	@Autowired
	private RestTemplate restTemplate;

	@Bean
	@LoadBalanced
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Override
	public Booking makeBooking(BookingDto bookingDto) {
		Booking booking = new Booking();
		BeanUtils.copyProperties(bookingDto, booking);
		booking.setPassengers(bookingDto.getPassengers());

		User user = userRepo.findByEmail(bookingDto.getEmailId());
		if (user == null) {
				throw new UserNotfoundException("requested user is not there");
			
		}

			Optional<Flight> option = flightRepo.findById(bookingDto.getFlightId());
			Flight flight=null;
			if (option.isPresent()) {
				flight = option.get();

			} else {
				throw new FlightNotFoundException("requested flight is not there");
			}
			
			
			if (flight.getNoOfSeatsAvailable() > bookingDto.getNoOfSeatsBooked()) {
				double fare = flight.getFare() * bookingDto.getNoOfSeatsBooked();

				booking.setAmount(fare);

				ResponseEntity<String> resposne = this.transction(bookingDto, fare);
				if (resposne.getStatusCode() == HttpStatus.OK) {

					flight.setNoOfSeatsAvailable(flight.getNoOfSeatsAvailable() - bookingDto.getNoOfSeatsBooked());
					flightRepo.save(flight);

					booking.setUser(user);
					booking.setFlight(flight);
					booking.setJourneyDate(flight.getJourneyDate());

					return bookingRepo.save(booking);
				}
				else {
					throw new TransactionFailedException();
				}
			} 
			else {
				throw new SeatsNotAvilableException();
			}
		} 
		

	

	public ResponseEntity<String> transction(BookingDto bookingDto, double sum) {
		String uri = "http://BANKING-SERVICE/transaction";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("accountNumber", bookingDto.getAccountNumber());
		request.put("amount", sum);
		request.put("banificiaryAccountNo", 6666);

		HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);

		try {
		

			return  restTemplate.postForEntity(uri, entity, String.class);
		} catch (Exception e) {

			throw new UserNotfoundException("Transaction details are not correct");
		}
	}
	
	
	@Override
	public Booking getBookingById(int bookingId) {
			
		Optional<Booking> option = bookingRepo.findById(bookingId);
			Booking booking = null;
			if (option.isPresent()) {
				booking = option.get();

			} else {
				throw new BookingNotfoundException("Booking with id: " + bookingId + "  Not found");
			}
			return booking;
		}	
		
	}


