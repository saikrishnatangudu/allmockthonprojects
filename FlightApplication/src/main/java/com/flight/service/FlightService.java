package com.flight.service;

import java.util.List;

import com.flight.dto.FlightDto;
import com.flight.model.Flight;

public interface FlightService {

	List<Flight> searchFlight(FlightDto flightDto);

}
