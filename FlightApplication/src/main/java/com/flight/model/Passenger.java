package com.flight.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table
public class Passenger {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long passengerId;
	@JsonIgnore
	private String email;
	@JsonIgnore
	private String password;


	/*
	 * @ManyToOne(cascade = CascadeType.ALL)
	 * 
	 * @JsonIgnore private Booking bookings;
	 */

	public Long getPassengerId() {
		return passengerId;
	}

	public void setPassengerId(Long passengerId) {
		this.passengerId = passengerId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	/*
	 * public Booking getBookings() { return bookings; }
	 * 
	 * 
	 * 
	 * 
	 * public void setBookings(Booking bookings) { this.bookings = bookings; }
	 */

}