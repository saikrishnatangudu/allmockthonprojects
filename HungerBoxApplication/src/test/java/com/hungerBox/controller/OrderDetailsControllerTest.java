package com.hungerBox.controller;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.hungerbox.controller.OrderDetailsController;
import com.hungerbox.dto.EmployeeOrder;
import com.hungerbox.model.Employee;
import com.hungerbox.service.OrderDetailsService;




@RunWith(MockitoJUnitRunner.Silent.class)
public class OrderDetailsControllerTest {
	@InjectMocks
	OrderDetailsController orderDetailsController;

	@Mock
	OrderDetailsService orderDetailsService;

	@Test
	public void TestorderListForPositive() {
		EmployeeOrder employee = new EmployeeOrder();
		Employee emp =  new Employee();
		emp.setEmployeeId(1L);
		emp.setEmployeeName("siva");
		Mockito.when(orderDetailsService.orderList(Mockito.anyLong())).thenReturn(employee);
		ResponseEntity<EmployeeOrder> order = orderDetailsController.orderList(Mockito.anyLong());
		assertNotNull(order);
	}
	@Test
	public void TestorderListForNagative() {
		EmployeeOrder employee = new EmployeeOrder();
		Employee emp =  new Employee();
		emp.setEmployeeId(-1L);
		emp.setEmployeeName("siva");
		Mockito.when(orderDetailsService.orderList(Mockito.anyLong())).thenReturn(employee);
		ResponseEntity<EmployeeOrder> order = orderDetailsController.orderList(Mockito.anyLong());
		assertNotNull(order);
	}
}
