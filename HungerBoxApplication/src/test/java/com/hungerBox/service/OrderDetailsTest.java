package com.hungerBox.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hungerbox.dto.EmployeeOrder;
import com.hungerbox.exception.EmployeeNotFoundException;
import com.hungerbox.model.Employee;
import com.hungerbox.repository.EmployeeRepository;
import com.hungerbox.repository.ItemRepository;
import com.hungerbox.repository.OrderItemListRepository;
import com.hungerbox.service.OrderDetailsServiceImpl;
import com.hungerbox.service.SearchServiceImpl;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OrderDetailsTest {

	@InjectMocks
	OrderDetailsServiceImpl orderDetailsServiceImpl;

	@Mock
	EmployeeRepository employeeRepository;
	
	@Mock
	OrderItemListRepository orderItemListRepository;
	
	@Test(expected = EmployeeNotFoundException.class)
	public void testOrderService () {
		Employee employee = new Employee();
		employee.setEmail("gs@gmail.com");
		employee.setEmployeeId(1L);
		employee.setEmployeeName("gs");
		employee.setPassword("pwd");
		employee.setPhone("9876543210");
		//EmployeeOrder employeeOrder = new EmployeeOrder();
		
		Mockito.when(employeeRepository.findById(2l).orElseThrow(()->new EmployeeNotFoundException("employee with given id not found"))).thenReturn(employee);
	EmployeeOrder employeeOrder=orderDetailsServiceImpl.orderList(2l);
	Assert.assertNotNull(employeeOrder);
	
	
	
	}
	
	

}
