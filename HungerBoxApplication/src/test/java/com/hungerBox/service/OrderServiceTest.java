package com.hungerBox.service;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.hungerbox.dto.TransactionDto;
import com.hungerbox.repository.EmployeeRepository;
import com.hungerbox.repository.ItemRepository;
import com.hungerbox.repository.OrderItemListRepository;
import com.hungerbox.repository.OrderRepository;
import com.hungerbox.repository.VendorRepository;
import com.hungerbox.service.OrderService;


@RunWith(MockitoJUnitRunner.Silent.class)
public class OrderServiceTest {
	
	@InjectMocks
	OrderService orderService;

	@Mock
	EmployeeRepository employeeRepository;
	
	@Mock
	OrderItemListRepository orderItemListRepository;
	
	@Mock
	OrderRepository orderRepository;

	@Mock
	 ItemRepository itemRepository;

	@Mock
	VendorRepository vendorRepository;
	@Autowired
	RestTemplate restTemplate;

	
	
	@Test
	public void booResponseEntitypos() {
		TransactionDto transactionDto = new TransactionDto();
		transactionDto.setAmount(222);
		transactionDto.setPhoneNo("9999");
		transactionDto.setToPhoneNo("99999");
		String uri = "http://BANKING-SERVICE/transaction";
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		JSONObject request = new JSONObject();
		request.put("phoneNo", transactionDto.getPhoneNo());
		request.put("amount", transactionDto.getAmount());
		request.put("toPhoneNo", transactionDto.getToPhoneNo());

		HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);
		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);
		Assert.assertEquals(HttpStatus.OK,response.getStatusCode());
	}


}
