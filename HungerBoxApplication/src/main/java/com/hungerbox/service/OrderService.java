package com.hungerbox.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hungerbox.dto.OrderDto;
import com.hungerbox.dto.ResponseDto;
import com.hungerbox.dto.TransactionDto;
import com.hungerbox.exception.DataNotFoundException;
import com.hungerbox.model.Employee;
import com.hungerbox.model.Item;
import com.hungerbox.model.Order;
import com.hungerbox.model.OrderItemList;
import com.hungerbox.model.Vendor;
import com.hungerbox.repository.EmployeeRepository;
import com.hungerbox.repository.ItemRepository;
import com.hungerbox.repository.OrderItemListRepository;
import com.hungerbox.repository.OrderRepository;
import com.hungerbox.repository.VendorRepository;

@Service
public class OrderService {

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	OrderItemListRepository orderItemListRepository;

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	VendorRepository vendorRepository;

	@Autowired
	RestTemplate restTemplate;

	@Bean
	@LoadBalanced
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	public ResponseDto saveOrder(OrderDto orderDto) {

		Employee employee = employeeRepository.findById(orderDto.getEmployeeId())
				.orElseThrow(() -> new DataNotFoundException("Employee not found "));

		List<Long> itemIds = orderDto.getItemDto().stream().map(itemId -> itemId.getItemId())
				.collect(Collectors.toList());

		List<Integer> quantityList = orderDto.getItemDto().stream().map(quantity -> quantity.getQuantity())
				.collect(Collectors.toList());

		Vendor vendor = vendorRepository.findById(orderDto.getVendorId())
				.orElseThrow(() -> new DataNotFoundException("Vendor not found"));

		double[] orderAmount = { 0 };
		int[] i = { 0 };
		/*
		 * for (int i = 0; i < itemIds.size(); i++) { Item item =
		 * itemRepository.findByItemIdAndVendor(itemIds.get(i), vendor); if (item ==
		 * null) { throw new
		 * DataNotFoundException("item you have given is not provided by the vendor"); }
		 * 
		 * orderAmount = orderAmount + (quantityList.get(i) * item.getUnitPrice());
		 * 
		 * }
		 */
		itemIds.stream().forEach(id -> {
			System.out.println("itemid"+id);
			Item item = itemRepository.findByItemIdAndVendor(id, vendor);
			if (item == null) {
				throw new DataNotFoundException("item you have given is not provided by the vendor");
			}

			orderAmount[0] = orderAmount[0] + (quantityList.get(i[0]) * item.getUnitPrice());
			
			System.out.println("quantity"+quantityList.get(i[0]));
			System.out.println("unitprice"+item.getUnitPrice());
			System.out.println(orderAmount[0]);
	
		});

		TransactionDto transactionDto = new TransactionDto();
		transactionDto.setPhoneNo(orderDto.getPhoneNo());
		transactionDto.setToPhoneNo("9490037089");
		transactionDto.setAmount(orderAmount[0]);

		try {
			this.transction(transactionDto);
		} catch (Exception e) {
			throw new DataNotFoundException("Transaction is failure");
		}

		Order[] order = {new Order()};
		order[0].setEmployee(employee);
		order[0].setOrderPlacedAt(LocalDateTime.now());
		order[0] = orderRepository.save(order[0]);

		/*
		 * for (int i = 0; i < itemIds.size(); i++) { Item item =
		 * itemRepository.findById(itemIds.get(i)) .orElseThrow(() -> new
		 * DataNotFoundException("item is not found")); OrderItemList orderItemList =
		 * new OrderItemList(); orderItemList.setOrder(order);
		 * orderItemList.setQuantity(quantityList.get(i)); orderItemList.setItem(item);
		 * orderItemList.setEmployee(employee);
		 * 
		 * orderItemListRepository.save(orderItemList); }
		 */
		int[] k = { 0 };
		itemIds.stream().forEach(id -> {
			System.out.println(id);
			Item item = itemRepository.findById(id)
					.orElseThrow(() -> new DataNotFoundException("item is not found"));
			
			OrderItemList orderItemList = new OrderItemList();
			orderItemList.setOrder(order[0]);
			orderItemList.setQuantity(quantityList.get(k[0]));
			orderItemList.setItem(item);
			orderItemList.setEmployee(employee);
			k[0]++;
			orderItemListRepository.save(orderItemList);
		});

		order[0].setOrderPrice(orderAmount[0]);
		orderRepository.save(order[0]);
		ResponseDto responseDto = new ResponseDto();
		responseDto.setEmail(employee.getEmail());
		responseDto.setEmployeeName(employee.getEmployeeName());
		responseDto.setOrderPlacedAt(LocalDateTime.now());
		responseDto.setOrderAmount(orderAmount[0]);
		responseDto.setPhone(orderDto.getPhoneNo());

		responseDto
				.setMessage("<<HungerBox>>  order confirmed.....ThankYou for your Order " + employee.getEmployeeName());
		return responseDto;
	}

	public ResponseEntity<String> transction(TransactionDto transactionDto) {
		String uri = "http://BANKING-SERVICE/payment";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("phoneNo", transactionDto.getPhoneNo());
		request.put("amount", transactionDto.getAmount());
		request.put("toPhoneNo", transactionDto.getToPhoneNo());

		HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);

		return restTemplate.postForEntity(uri, entity, String.class);
	}

}
