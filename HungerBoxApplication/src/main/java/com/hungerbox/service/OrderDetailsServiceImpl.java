package com.hungerbox.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungerbox.dto.EmployeeOrder;
import com.hungerbox.exception.EmployeeNotFoundException;
import com.hungerbox.model.Employee;
import com.hungerbox.model.OrderItemList;
import com.hungerbox.repository.EmployeeRepository;
import com.hungerbox.repository.ItemRepository;
import com.hungerbox.repository.OrderItemListRepository;
import com.hungerbox.repository.OrderRepository;
import com.hungerbox.repository.VendorRepository;



@Service
public class OrderDetailsServiceImpl implements OrderDetailsService {

	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	ItemRepository itemRepository;
	@Autowired
	OrderItemListRepository orderItemRepository;
	@Autowired
	OrderRepository orderRepository;
	@Autowired
	VendorRepository vendorRepository;

	@Override
	public EmployeeOrder orderList(long employeeId)  {

		Employee employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new EmployeeNotFoundException("Employee with give id not found"));

		List<OrderItemList> order = orderItemRepository.findByEmployee(employee);
		
		if(order.isEmpty()) {
			throw new EmployeeNotFoundException("No orders for this employee");
		}
		
		List<OrderItemList> orders =order.stream().sorted(Comparator.comparing(OrderItemList::getId).reversed()).collect(Collectors.toList());
		//int count=0;
		List<OrderItemList> orderItemList= new ArrayList<>();
		
		orders.stream().forEach(od->{
			orderItemList.add(od);
			
		});
		EmployeeOrder employeeOrder = new EmployeeOrder();
		employeeOrder.setEmployee(employee);
		employeeOrder.setOrders(orderItemList.stream().limit(5).collect(Collectors.toList()));
		
		return employeeOrder;
        	 
         
	

	}
}
