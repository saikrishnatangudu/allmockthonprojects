package com.hungerbox.exception;

import java.security.Timestamp;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdviseHandler extends ResponseEntityExceptionHandler {

	/*
	 * @ExceptionHandler(EmployeeNotFoundException.class) public
	 * ResponseEntity<Object> exception(EmployeeNotFoundException exception) {
	 * Map<String, Object> body = new LinkedHashMap<>(); body.put("timestamp",
	 * LocalDateTime.now()); body.put("message", exception.getMessage()); return new
	 * ResponseEntity<>(body, HttpStatus.NOT_FOUND); }
	 */
	@ExceptionHandler(EmployeeNotFoundException.class)
	public ResponseEntity<Object> handleUserNotFoundException1(EmployeeNotFoundException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", ex.getMessage());

		return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = ItemNotFoundException.class)
	public ResponseEntity<Object> exception(ItemNotFoundException exception) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", exception.getMessage());
		return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
	}
}
