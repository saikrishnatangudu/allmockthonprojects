package com.hungerbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hungerbox.model.Employee;



public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	

}
