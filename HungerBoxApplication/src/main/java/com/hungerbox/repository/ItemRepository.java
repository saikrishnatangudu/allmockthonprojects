package com.hungerbox.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hungerbox.model.Item;
import com.hungerbox.model.Vendor;



public interface ItemRepository extends JpaRepository<Item, Long> {
	
	Item findByItemIdAndVendor(Long itemId,Vendor vendor);

	List<Item> getItemByNameLike(String string);

}
