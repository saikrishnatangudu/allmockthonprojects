package com.hungerbox.dto;


import java.util.List;

public class OrderDto {
	private Long employeeId;
	private List<ItemDto> itemDto;
	private String PhoneNo;
	private Long vendorId;
	
	
	
	public Long getVendorId() {
		return vendorId;
	}
	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}
	public String getPhoneNo() {
		return PhoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		PhoneNo = phoneNo;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public List<ItemDto> getItemDto() {
		return itemDto;
	}
	public void setItemDto(List<ItemDto> itemDto) {
		this.itemDto = itemDto;
	}
	
}
