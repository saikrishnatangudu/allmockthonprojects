package com.learn.Ecommerce1.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table
public class Store {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer storeId;
	
	private Double rating;
	
	private String location;
	private String name;
	
	
	
	    @OneToMany( cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	    private List<StoreReview> storeReviewList;
	    @OneToMany( cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	    private List<Order> orders;

	}
