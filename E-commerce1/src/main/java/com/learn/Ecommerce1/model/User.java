package com.learn.Ecommerce1.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import net.bytebuddy.implementation.bind.annotation.IgnoreForBinding;

@Entity
@Table
public class User{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long UserId;
	
	@JsonIgnore
	private String username;
	
	@JsonIgnore
	private String email;

	/*
	 * @OneToMany( cascade = CascadeType.MERGE, fetch = FetchType.LAZY) private
	 * List<StoreReview> storeReviewList;
	 * 
	 * @OneToMany( cascade = CascadeType.MERGE, fetch = FetchType.LAZY) private
	 * List<ProductReview> productReviewList;
	 * 
	 * @OneToMany( cascade = CascadeType.MERGE, fetch = FetchType.LAZY) private
	 * List<Order> orders;
	 */


	}
