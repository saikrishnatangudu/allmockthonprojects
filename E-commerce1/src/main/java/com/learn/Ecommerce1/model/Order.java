package com.learn.Ecommerce1.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "orders")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    	private int id;
	    private double price;
	    @JoinColumn(name = "productId")
		@OneToMany( cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
		private List< Product> product;
	    @JoinColumn(name = "userId")
	   	@ManyToOne( cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	   	private User user;
	    @JoinColumn(name = "storeId")
		@ManyToOne( cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
		private Store Store;
	    
	}
