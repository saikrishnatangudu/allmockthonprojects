package com.learn.Ecommerce1.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table
public class StoreReview {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer storeReviewId;
	private Double rating;
	private String feedBack;

	@JoinColumn(name = "storeId")
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	private Store Store;
	@JoinColumn(name = "userId")
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	private User user;

	public Integer getStoreReviewId() {
		return storeReviewId;
	}

	public void setStoreReviewId(Integer storeReviewId) {
		this.storeReviewId = storeReviewId;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public String getFeedBack() {
		return feedBack;
	}

	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}

	public Store getStore() {
		return Store;
	}

	public void setStore(Store store) {
		Store = store;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
