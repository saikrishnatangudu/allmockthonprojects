package com.bus.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;

import com.bus.dto.BusDto;
import com.bus.dto.CreateBusDto;
import com.bus.exception.BusNotFoundException;
import com.bus.model.Bus;
import com.bus.repository.BusRepo;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BusServiceImplTest {
	@InjectMocks
	BusServiceImpl userServiceImpl;

	@Mock
	BusRepo busRepository;

	static Bus bus = null;

	@BeforeClass
	public static void setUp() {
		bus = new Bus();
	}

	@Test(expected = BusNotFoundException.class)
	public void testsearchbusForPositive() {
		List<Bus> buses = new ArrayList();
		Bus bus = new Bus();
		BusDto busDto =new BusDto();
		bus.setBusId(1);
		//bus.setBusname("boopalbus");
		bus.setDestination("bangalore");
		bus.setSource("salem");
		buses.add(bus);
		Mockito.when(busRepository.findBusBySourceAndDestinationAndJourneyDate("salem", "bangalore", null)).thenReturn(buses);
		List<Bus> buss = userServiceImpl.searchbus(busDto);
		Assert.assertNotNull(buss);
		Assert.assertEquals(1, buss.size());

	}

	@Test
	public void testsearchbusForNagative() {
		List<Bus> buses = new ArrayList();
		Bus bus = new Bus();
		bus.setBusId(-1);
		BusDto busDto =new BusDto();
		//bus.setBusname("boopalbus");
		busDto.setDestination("bangalore");
		busDto.setSource("salem");
		busDto.setJourneyDate(null);
		buses.add(bus);
		Mockito.when(busRepository.findBusBySourceAndDestinationAndJourneyDate("salem", "bangalore", null)).thenReturn(buses);
		List<Bus> buss = userServiceImpl.searchbus(busDto);
		Assert.assertNotNull(buss);
		Assert.assertEquals(1, buss.size());
		
	}

	
	@Test
	public void testcrateBus() {
		List<Bus> buses = new ArrayList();
		Bus bus = new Bus();
		
		BusDto busDto =new BusDto();
		busDto.setDestination("bangalore");
		busDto.setSource("salem");
		busDto.setJourneyDate(null);
		buses.add(bus);
		CreateBusDto createBusDto = new CreateBusDto();
		BeanUtils.copyProperties(createBusDto, bus);
		Mockito.when(busRepository.save(bus)).thenReturn(bus);
		Bus buss = userServiceImpl.createBus(createBusDto);
		Assert.assertEquals(null, buss);
		
	}
	
	
	
}
