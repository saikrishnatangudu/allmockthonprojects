package com.bus.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.bus.dto.UserDTO;
import com.bus.model.User;
import com.bus.service.BookListImpl;


@RunWith(MockitoJUnitRunner.Silent.class)
public class BookingListTest {

	@InjectMocks
	private BookingList userController;

	@Mock
	private BookListImpl userService;

	@Test(expected = NullPointerException.class)
	public void testFindByIdForPositive() {
		User user = new User(1, "jansi", "Jansi", "jansi@gmail.com", "9566497854", "Female");
		Mockito.when(userService.getUserById(1)).thenReturn(user);
		UserDTO user1 = userController.getUserById(1);
		Assert.assertEquals("jansi@gmail.com", user1.getEmail());
	}

	@Test(expected = NullPointerException.class)
	public void testFindByIdForNegative() {
		User user = new User(-1, "jansi", "Jansi", "jansi@gmail.com", "9566497854", "Female");
		Mockito.when(userService.getUserById(-1)).thenReturn(user);
		UserDTO user1 = userController.getUserById(-1);
		Assert.assertEquals("jansi@gmail.com", user1.getEmail());
	}

}
