package com.bus.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bus.dto.BusDto;
import com.bus.dto.CreateBusDto;
import com.bus.dto.ResponseMessageDto;
import com.bus.model.Bus;
import com.bus.model.User;
import com.bus.service.BusService;
import com.bus.service.UserService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BusControllerTest {
	@InjectMocks
	BusController busController;
	@Mock
	BusService busService;
	@Mock
	UserService userService;
	MockMvc mockMvc;
	static Bus bus = null;
	static User user = null;
	static CreateBusDto createBusDto = null;
	static List<Bus> busses = null;

	@Before
	public void setUp() {

		mockMvc = MockMvcBuilders.standaloneSetup(busController).build();

	}

	@BeforeClass
	public static void setup() {
		bus = new Bus();
		bus.setBusId(1);
		bus.setBusName("pk");
		user = new User();
		user.setEmail("sk");
		user.setRole("kk");
		createBusDto = new CreateBusDto();
		busses = new ArrayList<Bus>();
		busses.add(bus);

	}

	@Test
	public void testcreateBuspositive() {
		ResponseMessageDto dto = new ResponseMessageDto();

		Mockito.when(userService.finduserByEmail(user.getEmail())).thenReturn(true);

		Mockito.when(busService.createBus(createBusDto)).thenReturn(bus);

		ResponseEntity<ResponseMessageDto> p1 = busController.createBus(createBusDto, user.getEmail());
		Assert.assertNotNull(p1);
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertEquals(HttpStatus.OK, p1.getStatusCode());

	}

	@Test
	public void testcreateBusNeg() {
		ResponseMessageDto dto = new ResponseMessageDto();

		Mockito.when(userService.finduserByEmail(user.getEmail())).thenReturn(false);

		Mockito.when(busService.createBus(createBusDto)).thenReturn(bus);

		ResponseEntity<ResponseMessageDto> p1 = busController.createBus(createBusDto, user.getEmail());
		Assert.assertNotNull(p1);
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, p1.getStatusCode());

	}

	@Test
	public void testupdateBuspositive() {
		ResponseMessageDto dto = new ResponseMessageDto();

		Mockito.when(userService.finduserByEmail(user.getEmail())).thenReturn(true);

		Mockito.when(busService.createBus(createBusDto)).thenReturn(bus);

		ResponseEntity<ResponseMessageDto> p1 = busController.updateBus(createBusDto, user.getEmail());
		Assert.assertNotNull(p1);
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertEquals(HttpStatus.OK, p1.getStatusCode());

	}

	@Test
	public void testupdateBusNeg() {
		ResponseMessageDto dto = new ResponseMessageDto();

		Mockito.when(userService.finduserByEmail(user.getEmail())).thenReturn(false);

		Mockito.when(busService.createBus(createBusDto)).thenReturn(bus);

		ResponseEntity<ResponseMessageDto> p1 = busController.updateBus(createBusDto, user.getEmail());
		Assert.assertNotNull(p1);
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, p1.getStatusCode());

	}

	@Test
	public void testdeleteposve() {
		ResponseMessageDto dto = new ResponseMessageDto();

		Mockito.when(userService.finduserByEmail(user.getEmail())).thenReturn(true);

		Mockito.when(busService.deleteBus(bus.getBusId())).thenReturn(0);

		ResponseEntity<ResponseMessageDto> p1 = busController.deleteBus(0, "sk");
		Assert.assertNotNull(p1);
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertEquals(HttpStatus.OK, p1.getStatusCode());

	}

	@Test
	public void testdeleteponve() {
		ResponseMessageDto dto = new ResponseMessageDto();

		Mockito.when(userService.finduserByEmail(user.getEmail())).thenReturn(false);
		Mockito.when(busService.deleteBus(bus.getBusId())).thenReturn(0);

		ResponseEntity<ResponseMessageDto> p1 = busController.deleteBus(0, "sk");
		Assert.assertNotNull(p1);
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, p1.getStatusCode());

	}

	@Test
	public void testlistBuspos() {
		ResponseMessageDto dto = new ResponseMessageDto();

		Mockito.when(userService.finduserByEmail(user.getEmail())).thenReturn(true);
		Mockito.when(busService.getBus()).thenReturn(busses);

		ResponseEntity<Object> p1 = busController.getBus("sk");
		Assert.assertNotNull(p1);
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertEquals(HttpStatus.OK, p1.getStatusCode());

	}

	@Test
	public void testbuslisneg() {
		ResponseMessageDto dto = new ResponseMessageDto();

		Mockito.when(userService.finduserByEmail(user.getEmail())).thenReturn(false);
		Mockito.when(busService.getBus()).thenReturn(busses);

		ResponseEntity<Object> p1 = busController.getBus("sk");
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertNotNull(p1);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, p1.getStatusCode());

	}

	@Test
	public void testlistbysourseps() {
		ResponseMessageDto dto = new ResponseMessageDto();
		BusDto busDto = new BusDto();
		Mockito.when(userService.finduserByEmail(user.getEmail())).thenReturn(true);
		Mockito.when(busService.searchbus(busDto)).thenReturn(busses);

		ResponseEntity<Object> p1 = busController.searchbus(busDto, "sk");
		Assert.assertNotNull(p1);
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertEquals(HttpStatus.OK, p1.getStatusCode());

	}

	@Test
	public void testbuslistbysrcneg() {
		ResponseMessageDto dto = new ResponseMessageDto();
		BusDto busDto = new BusDto();
		Mockito.when(userService.finduserByEmail(user.getEmail())).thenReturn(false);
		Mockito.when(busService.searchbus(busDto)).thenReturn(busses);

		ResponseEntity<Object> p1 = busController.searchbus(busDto, "sk");
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertNotNull(p1);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, p1.getStatusCode());

	}

}
