package com.bus.service;

import java.util.List;

import com.bus.dto.BusDto;
import com.bus.dto.CreateBusDto;
import com.bus.model.Bus;

public interface BusService {

	public Bus createBus(CreateBusDto createBusDto);

	public List<Bus> searchbus(BusDto busDto);

	public int deleteBus(int id);

	public List<Bus> getBus();

}
