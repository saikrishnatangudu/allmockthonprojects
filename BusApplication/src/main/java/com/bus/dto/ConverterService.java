package com.bus.dto;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.bus.model.User;

@Component
public class ConverterService {

	@Autowired
	private ModelMapper modelMapper;

	public UserDTO convertToDto(User userObject) {
		return modelMapper.map(userObject, UserDTO.class);
	}

}
