package com.pay.Service;



import com.pay.Dto.ResponseDto;
import com.pay.Dto.UserDto;
import com.pay.Model.User;

public interface UserService {

	ResponseDto createUser(UserDto userDto);

	User getuserlists(String phone);

}
