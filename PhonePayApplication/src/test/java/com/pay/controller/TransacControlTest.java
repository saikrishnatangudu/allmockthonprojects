package com.pay.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.pay.Controller.TransactionController;
import com.pay.Dto.TransactionDto;
import com.pay.Dto.TransactionResponseDto;
import com.pay.Service.TransacServiceImpl;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TransacControlTest {
@InjectMocks
TransactionController transactionController;

@Mock
TransacServiceImpl  transacServiceImpl;


@Test
public void checkTransaction() {
	
	TransactionDto t = new TransactionDto();
	TransactionResponseDto td = new TransactionResponseDto();
	
	Mockito.when(transacServiceImpl.createTransac(t)).thenReturn(td);
	
	ResponseEntity<TransactionResponseDto> ydc=transactionController.transaction(t);
	Assert.assertNotNull(ydc);
	Assert.assertEquals(HttpStatus.OK, ydc.getStatusCode());
	
	
}



}
