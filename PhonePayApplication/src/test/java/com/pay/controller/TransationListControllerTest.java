package com.pay.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.pay.Controller.TransactionController;
import com.pay.Controller.UserController;
import com.pay.Model.User;
import com.pay.Service.TransacService;
import com.pay.Service.TransacServiceImpl;
import com.pay.Service.UserServiceImpl;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TransationListControllerTest {
	@InjectMocks
	UserController transationListController;
	@Mock
	UserServiceImpl transationListService;
	static User user = null;
	@BeforeClass
	public static void setUp() {
		user = new User();
	}
	@Test
	public void testgetuserlistForPositive() {
		List<User> users = new ArrayList();
		User user = new User();
		user.setPhone("8838662182");
		user.setGender("male");
		user.setName("siva");
		user.setDOB(null);
		user.setUserId(null);
		Mockito.when(transationListService.getuserlists(null)).thenReturn(user);
		ResponseEntity<User> userr = transationListController.getuser(null);
		Assert.assertNotNull(userr);
		Assert.assertEquals(userr.getStatusCode(), HttpStatus.OK);
	}
	@Test
	public void testgetuserlistForNagative() {
		List<User> users = new ArrayList();
		User user = new User();
		user.setPhone("-8838662182");
		user.setGender("male");
		user.setName("siva");
		user.setDOB(null);
		user.setUserId(null);
		Mockito.when(transationListService.getuserlists(null)).thenReturn(user);
		ResponseEntity<User> userr = transationListController.getuser(null);
		Assert.assertNotNull(userr);
		Assert.assertEquals(userr.getStatusCode(), HttpStatus.OK);
	}	
	
	@Test
	public void testgetuserlistForPitive() {
		List<User> users = new ArrayList();
		User user = new User();
		user.setPhone("8838662182");
		user.setGender("male");
		user.setName("siva");
		user.setDOB(null);
		user.setUserId(null);
		Mockito.when(transationListService.getuserlists(null)).thenReturn(user);
		ResponseEntity<User> userr = transationListController.getuser(null);
		Assert.assertNotNull(userr);
		Assert.assertEquals(userr.getStatusCode(), HttpStatus.OK);
	}
	@Test
	public void testgetuserltForNagative() {
		List<User> users = new ArrayList();
		User user = new User();
		user.setPhone("-8838662182");
		user.setGender("male");
		user.setName("siva");
		user.setDOB(null);
		user.setUserId(null);
		Mockito.when(transationListService.getuserlists(null)).thenReturn(user);
		ResponseEntity<User> userr = transationListController.getuser(null);
		Assert.assertNotNull(userr);
		Assert.assertEquals(userr.getStatusCode(), HttpStatus.OK);
	}
	
	
	@Test
	public void testgetuserlitForPositive() {
		List<User> users = new ArrayList();
		User user = new User();
		user.setPhone("8838662182");
		user.setGender("male");
		user.setName("siva");
		user.setDOB(null);
		user.setUserId(null);
		Mockito.when(transationListService.getuserlists(null)).thenReturn(user);
		ResponseEntity<User> userr = transationListController.getuser(null);
		Assert.assertNotNull(userr);
		Assert.assertEquals(userr.getStatusCode(), HttpStatus.OK);
	}
	@Test
	public void testgetuserlisForNagative() {
		List<User> users = new ArrayList();
		User user = new User();
		user.setPhone("-8838662182");
		user.setGender("male");
		user.setName("siva");
		user.setDOB(null);
		user.setUserId(null);
		Mockito.when(transationListService.getuserlists(null)).thenReturn(user);
		ResponseEntity<User> userr = transationListController.getuser(null);
		Assert.assertNotNull(userr);
		Assert.assertEquals(userr.getStatusCode(), HttpStatus.OK);
	}
	@AfterClass
	public static void tearDown() {
		user = null;
	}
}