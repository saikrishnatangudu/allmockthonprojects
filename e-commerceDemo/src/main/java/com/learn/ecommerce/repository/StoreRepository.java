package com.learn.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.learn.ecommerce.model.Store;



@Repository
public interface StoreRepository extends JpaRepository<Store, Long> {
	
	

}
