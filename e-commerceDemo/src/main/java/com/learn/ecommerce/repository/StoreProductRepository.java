package com.learn.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.ecommerce.model.Product;
import com.learn.ecommerce.model.Store;
import com.learn.ecommerce.model.Store_Product;

public interface StoreProductRepository extends JpaRepository<Store_Product, Long> {
	
	List<Store_Product> findByProduct(Product product);
	
	List<Store_Product> findByStore(Store store);
	List<Store_Product> findStoreByProduct(Product product);
}
