package com.learn.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.ecommerce.model.Product;
import com.learn.ecommerce.model.Product_rating;

public interface ProductRepository extends JpaRepository<Product, Long> {

	List<Product> findByNameLike(String productName);
	Product findByName(String productName);
	
	

}
