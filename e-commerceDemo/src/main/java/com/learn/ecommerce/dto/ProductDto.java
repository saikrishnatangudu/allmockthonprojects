package com.learn.ecommerce.dto;

import java.util.List;

import com.learn.ecommerce.model.Product;
import com.learn.ecommerce.model.Product_rating;
import com.learn.ecommerce.model.Store;

public class ProductDto {
	
	private Product product;
	
	private List<Store> stores;
	
	private List<Product_rating> product_ratings;
	
	

	public List<Product_rating> getProduct_ratings() {
		return product_ratings;
	}

	public void setProduct_ratings(List<Product_rating> product_ratings) {
		this.product_ratings = product_ratings;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<Store> getStores() {
		return stores;
	}

	public void setStores(List<Store> stores) {
		this.stores = stores;
	}
	
	

}
