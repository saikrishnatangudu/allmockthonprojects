package com.learn.ecommerce.service;

import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.ecommerce.dto.ProductDto;
import com.learn.ecommerce.dto.ProductRatingDto;
import com.learn.ecommerce.dto.StoreReviewDto;
import com.learn.ecommerce.exceptions.OrderNotFoundException;
import com.learn.ecommerce.exceptions.ProductNotFoundException;
import com.learn.ecommerce.exceptions.UserNotFoundException;
import com.learn.ecommerce.model.Product;
import com.learn.ecommerce.model.Product_rating;
import com.learn.ecommerce.model.Store;
import com.learn.ecommerce.model.Store_Product;
import com.learn.ecommerce.model.Store_review;
import com.learn.ecommerce.model.User;
import com.learn.ecommerce.model.UserOrder;
import com.learn.ecommerce.repository.OrderRepository;
import com.learn.ecommerce.repository.ProductRepository;
import com.learn.ecommerce.repository.Product_ratingRepository;
import com.learn.ecommerce.repository.StoreProductRepository;
import com.learn.ecommerce.repository.StoreRepository;
import com.learn.ecommerce.repository.Store_reviewRepository;
import com.learn.ecommerce.repository.UserRepository;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;
	@Autowired
	StoreProductRepository storeProductRepository;
	@Autowired
	Store_reviewRepository store_reviewRepository;
	@Autowired
	StoreRepository storeRepository;
	@Autowired
	Product_ratingRepository product_ratingRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	OrderRepository orderRepository;

	public ProductDto viewProductByName(String productName) {

		Product product = productRepository.findByName(productName);
		if (product==null) {
			throw new ProductNotFoundException("product is not there");
		}

		List<Store_Product> sp = storeProductRepository.findStoreByProduct(product);
		List<Store> s = sp.stream().map(x -> x.getStore()).collect(Collectors.toList());
		List<Store> s1 = s.stream().sorted(Comparator.comparing(Store::getRating).reversed())
				.collect(Collectors.toList());
		System.out.println(s);
		System.out.println(s1);
		List<Product_rating> product_ratings = product_ratingRepository.findProduct_ratingByProduct(product);

		List<Product_rating> ratings = product_ratings.stream()
				.sorted(Comparator.comparing(Product_rating::getRating).reversed()).collect(Collectors.toList());
		System.out.println(ratings);

		ProductDto productDto = new ProductDto();
		productDto.setProduct(product);
		productDto.setStores(s1);
		productDto.setProduct_ratings(ratings);
		return productDto;

	}

	public Store_review storeRating(StoreReviewDto storeReviewDto) {
		User user = new User();

		user = userRepository.findById(storeReviewDto.getUserId())
				.orElseThrow(() -> new UserNotFoundException("user with given id not found"));
		Store str = storeRepository.findById(storeReviewDto.getStoreId())
				.orElseThrow(() -> new ProductNotFoundException("Store not found"));
		List<UserOrder> order = orderRepository.findOrderByUserAndStore(user, str);
		if (order != null) {

		Store store = storeRepository.getOne(storeReviewDto.getStoreId());

		Store_review store_review = new Store_review();
		store_review.setFeedback(storeReviewDto.getFeedback());
		store_review.setRating(storeReviewDto.getRating());
		store_review.setStore(store);
		store_review.setUser(user);
		store_reviewRepository.save(store_review);
		List<Store_review> store_reviews = store_reviewRepository.findStore_reviewsByStore(store);
		List<Double> ls = store_reviews.stream().map(x -> x.getRating()).collect(Collectors.toList());

		OptionalDouble avg = ls.stream().mapToDouble(Double::doubleValue).average();

		store.setRating(avg.getAsDouble());
		storeRepository.save(store);

		// store_reviewRepository.save(store_review);
		return store_review;

	}
		else {
			throw new OrderNotFoundException("order details not found-->you cannot review the product");
		}
	}

	public Product_rating productRating(ProductRatingDto productRatingDto) {

		User user = new User();

		user = userRepository.findById(productRatingDto.getUserId())
				.orElseThrow(() -> new UserNotFoundException("user with given id not found"));
		Product produ = productRepository.findById(productRatingDto.getProductId())
				.orElseThrow(() -> new ProductNotFoundException("product not found"));
		UserOrder order = orderRepository.findOrderByUserAndProduct(user, produ);
		if (order != null) {

			Product product = productRepository.getOne(productRatingDto.getProductId());

			Product_rating product_rating = new Product_rating();

			product_rating.setFeedback(productRatingDto.getFeedback());
			product_rating.setRating(productRatingDto.getRating());
			product_rating.setProduct(product);
			product_rating.setUser(user);
			product_ratingRepository.save(product_rating);
			List<Product_rating> product_ratings = product_ratingRepository.findProduct_ratingByProduct(product);

			List<Double> ls = product_ratings.stream().map(x -> x.getRating()).collect(Collectors.toList());
			System.out.println(ls);
			OptionalDouble avg = ls.stream().mapToDouble(Double::doubleValue).average();
			System.out.println(avg);
			product.setRating(avg.getAsDouble());
			productRepository.save(product);
			// product_ratingRepository.save(product_rating);
			return product_rating;
		} else {
			throw new OrderNotFoundException("order details not found-->you cannot review the product");
		}

	}
}
