package com.course.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.course.dto.EnrollResponseDto;
import com.course.service.EnrollService;

/**
 * 
 * @author saikrishna
 *
 */

@RestController
@RequestMapping("Enrollment")
public class EnrollController {
	Logger logger = LoggerFactory.getLogger(EnrollController.class);
	
	@Autowired
	EnrollService enrollService;

	@PostMapping("/enroll")
	public ResponseEntity<EnrollResponseDto> enrollTraining(@RequestParam Long employeeId ,@RequestParam Long batchId) {
		
		logger.info("in Enroll functionality");
	
		EnrollResponseDto enrollResponseDto = enrollService.enrollCourse(employeeId,batchId);
		return new ResponseEntity<>(enrollResponseDto, HttpStatus.OK);
	}

}
