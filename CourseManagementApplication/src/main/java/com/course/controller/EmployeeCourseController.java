package com.course.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.course.dto.TrainingResponseDto;
import com.course.service.EmployeeCourseService;
/**
 * 
 * @author saikrishna
 *
 */


@RestController
@RequestMapping("/courses")
public class EmployeeCourseController {
	@Autowired
	EmployeeCourseService employeeTrainingService;
	
	Logger logger = LoggerFactory.getLogger(EmployeeCourseService.class);
	
	@GetMapping("/search")
	public ResponseEntity<List<TrainingResponseDto>> getBatchs(@RequestParam String courseName){
		logger.info("in getbatchs");
		
		List<TrainingResponseDto>   trainingResponseDtos=employeeTrainingService.getAll(courseName);
		return new ResponseEntity<>(trainingResponseDtos,HttpStatus.OK);
	}

}
