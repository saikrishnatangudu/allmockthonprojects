package com.course.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.course.model.Batch;
import com.course.service.BatchService;
/**
 * 
 * @author saikrishna
 *
 */

@RestController
public class BatchController {
	@Autowired
	BatchService batchService;
	Logger logger = LoggerFactory.getLogger(BatchController.class);
	
	@GetMapping("/course")
	public ResponseEntity<List<Batch>>getCourses(@RequestParam String courseName){
		logger.info("in get courses method");
		List<Batch>	batchs=batchService.getAll(courseName);
		return new ResponseEntity<>(batchs,HttpStatus.OK);
	}

	
	
	

}
