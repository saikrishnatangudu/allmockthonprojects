package com.course.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.course.model.Batch;
import com.course.model.Course;
import com.course.model.Employee;
@Repository
public interface BatchRepository extends JpaRepository<Batch, Long>{

	List<Batch> findByCourse(Course course);

	List<Batch> findByCourseAndStartDateGreaterThanEqual(Course course, LocalDate date);
	
	List<Batch>  findCourseByEmployee(Employee employee);

}
