package com.course.dto;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TrainingResponseDto {
	
	private Long batchId;
	private String batchname;
	private LocalDate startDate;
	private LocalDate endDate;
	@JsonIgnore
	private String redgStatus;

	public Long getBatchId() {
		return batchId;
	}
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}
	public String getBatchname() {
		return batchname;
	}
	public void setBatchname(String batchname) {
		this.batchname = batchname;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	public String getRedgStatus() {
		return redgStatus;
	}
	public void setRedgStatus(String redgStatus) {
		this.redgStatus = redgStatus;
	}
	
	
}
