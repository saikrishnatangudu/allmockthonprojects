package com.course.dto;

import java.time.LocalDate;

public class EnrollResponseDto {
	private Long BatchId;
	private String batchname;
	private LocalDate startDate;
	private LocalDate endDate;
	private String courseName;
	private String message;
	
	public Long getBatchId() {
		return BatchId;
	}
	public void setBatchId(Long batchId) {
		BatchId = batchId;
	}

	public String getBatchname() {
		return batchname;
	}
	public void setBatchname(String batchname) {
		this.batchname = batchname;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
