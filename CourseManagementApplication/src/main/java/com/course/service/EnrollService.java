package com.course.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.course.dto.EnrollResponseDto;
import com.course.exception.DataNotFoundException;
import com.course.model.Batch;
import com.course.model.Employee;
import com.course.repository.BatchRepository;
import com.course.repository.EmployeeRepository;

@Service
public class EnrollService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	BatchRepository batchRepository;

	public EnrollResponseDto enrollCourse(Long employeeId, Long batchId) {
		LocalDate localDate = LocalDate.now();

		EnrollResponseDto enrollResponseDto = new EnrollResponseDto();

		Employee employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new DataNotFoundException("employee with given id not found"));

		Batch batch = batchRepository.findById(batchId)
				.orElseThrow(() -> new DataNotFoundException("training with given id not found"));

		if (batch.getEmployee().contains(employee)) {
			throw new DataNotFoundException("you are already enrolled for this batch");
		}

		List<Batch> batchs = batchRepository.findCourseByEmployee(employee);
		System.out.println(batchs);
		Optional<Batch> batch2 = batchs.stream().sorted(Comparator.comparing(Batch::getEndDate)).findFirst();
		System.out.println(!(batch2.isPresent()));
		// System.out.println(batch2.get());
		System.out.println(batch);

		if (!(batch2.isPresent()) || !(batch.getStartDate().isAfter(batch2.get().getEndDate()))) {
			System.out.println(batch + "......................................");
			if (batch.getStartDate().compareTo(localDate) > 0) {
				System.out.println("method");

				List<Employee> employees = batch.getEmployee();
				employeeRepository.save(employee);
				employees.add(employee);
				batch.setEmployee(employees);
				batchRepository.save(batch);

				BeanUtils.copyProperties(batch, enrollResponseDto);
				enrollResponseDto.setCourseName(batch.getCourse().getCourseName());
				enrollResponseDto.setMessage("successfully enrolled in " + batch.getCourse().getCourseName()
						+ " with batch name " + batch.getBatchname());
			} else {
				throw new DataNotFoundException("booking is not possible");
			}
		} else {
			throw new DataNotFoundException("you cannot enroll to previous batchs");
		}

		return enrollResponseDto;
	}

}
