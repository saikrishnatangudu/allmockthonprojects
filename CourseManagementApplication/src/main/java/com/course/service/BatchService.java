package com.course.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.course.exception.DataNotFoundException;
import com.course.model.Batch;
import com.course.model.Course;
import com.course.repository.BatchRepository;
import com.course.repository.CourseRepository;



@Service
public class BatchService {
	
	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	BatchRepository batchRepository;

	public List<Batch> getAll(String courseName){
		Course course=courseRepository.findByCourseName(courseName);
		if(course==null) {
			throw new DataNotFoundException("course is not there");
		}
		List<Batch> batch= batchRepository.findByCourse(course);
		if(batch==null) {
			throw new DataNotFoundException("batch with given course is not there");
		}

		
		List<Batch> batchs=batch.stream().sorted(Comparator.comparing(Batch::getStartDate)).collect(Collectors.toList());
		return batchs;
		
	}

	public Batch save(Batch batch) {
		
		return batchRepository.save(batch);
	}
	

}
