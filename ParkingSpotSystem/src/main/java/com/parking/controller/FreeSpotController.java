package com.parking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.parking.dto.FreeSpotDto;
import com.parking.dto.ReleaseDto;
import com.parking.service.SpotRequestService;

@RestController
public class FreeSpotController {
	
	@Autowired
	SpotRequestService spotRequestService;


	@PostMapping("/releaseRequest")
	public ResponseEntity<ReleaseDto> releaseRequest(@RequestBody FreeSpotDto freeSpotDto){
		ReleaseDto releaseDto = new ReleaseDto();
		
		spotRequestService.RaiseRequest(freeSpotDto);
		releaseDto.setMessage("raised sucessfully");
		return new ResponseEntity<>(releaseDto,HttpStatus.OK);
	}
	
	
}
