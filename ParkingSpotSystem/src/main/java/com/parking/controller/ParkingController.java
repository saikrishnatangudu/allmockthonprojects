package com.parking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.parking.dto.ReleaseDto;
import com.parking.model.Employee;
import com.parking.model.SpotRequest;
import com.parking.service.LotteryService;
import com.parking.service.ParkingService;

@Controller
public class ParkingController {

	@Autowired
	ParkingService parkingService;

	@Autowired
	LotteryService lotteryService;

	@GetMapping("/lottery")
	public ResponseEntity<ReleaseDto> lotterySystem() {
		ReleaseDto releaseDto = parkingService.saveParking();
		return new ResponseEntity<>(releaseDto, HttpStatus.OK);
	}

	@GetMapping("/parking/{empId}")
	public ResponseEntity<List<SpotRequest>> getParkingDetails(@PathVariable Long empId) {
		List<SpotRequest> s = parkingService.getEmployee(empId);
		return new ResponseEntity<>(s, HttpStatus.OK);
	}

	@PostMapping("/employee")
	public ResponseEntity<String> getParkingDetails(@RequestBody Employee employee) {
		parkingService.getsaveemp(employee);
		return new ResponseEntity<>("tfty", HttpStatus.OK);
	}

	@GetMapping("/lottnew")
	public ResponseEntity<ReleaseDto> lotterySystem1() {
		ReleaseDto releaseDto = lotteryService.saveParking();
		return new ResponseEntity<>(releaseDto, HttpStatus.OK);
	}

}
