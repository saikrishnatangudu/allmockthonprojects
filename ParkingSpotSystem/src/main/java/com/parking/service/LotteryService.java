package com.parking.service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parking.dto.ReleaseDto;
import com.parking.model.FreeSpot;
import com.parking.model.SpotRequest;
import com.parking.repository.EmployeeRepository;
import com.parking.repository.FreeSpotRepository;
import com.parking.repository.SpotRequestRepository;

@Service
public class LotteryService {
	
	@Autowired
	FreeSpotRepository freeSpotRepository;

	@Autowired
	SpotRequestRepository spotRequestRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;
	

	public ReleaseDto saveParking() {
		LocalDate date = LocalDate.now();
		List<SpotRequest> employeeReq = spotRequestRepository.findByStartDateGreaterThanEqualAndStatus(date,
				"not assigned");
		
		List<Long> spotrequestIds = employeeReq.stream().map(m -> m.getSpotRequesttId()).collect(Collectors.toList());
		
		List<FreeSpot> freeSpots = freeSpotRepository.findByStartDateGreaterThanEqualAndStatus(date, "not assigned");	
		
		List<Long> freeSpotIds = freeSpots.stream().map(spId-> spId.getFreeSpotId()).collect(Collectors.toList());
				
		
		for (Long spotrequestId : spotrequestIds) {
			SpotRequest spotRequest = spotRequestRepository.findById(spotrequestId).get();
			
			for (Long freeSpotId : freeSpotIds) {
				FreeSpot freeSpot = freeSpotRepository.findById(freeSpotId).get();
				
				if (freeSpot.getStartDate().equals(spotRequest.getStartDate())&&freeSpot.getStatus().equalsIgnoreCase("not assigned")&&spotRequest.getStatus().equalsIgnoreCase("not assigned")) {
					freeSpot.setStatus("assigned");
					spotRequest.setFreeSpot(freeSpot);
					spotRequest.setStatus("assigned");
					freeSpotRepository.save(freeSpot);
					spotRequestRepository.save(spotRequest);
					
					}
			}
        }

		ReleaseDto releaseDto = new ReleaseDto();
		releaseDto.setMessage("Assigned sucessfullly");
		
		return releaseDto;


}
}
