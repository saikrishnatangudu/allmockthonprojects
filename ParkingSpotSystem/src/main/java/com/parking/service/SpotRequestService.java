package com.parking.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parking.dto.FreeSpotDto;
import com.parking.dto.ReleaseDto;
import com.parking.exception.DataNotFoundException;
import com.parking.model.Employee;
import com.parking.model.EmployeeParkingLot;
import com.parking.model.FreeSpot;
import com.parking.repository.EmployeeParkingLotRepository;
import com.parking.repository.EmployeeRepository;
import com.parking.repository.FreeSpotRepository;

@Service
public class SpotRequestService {

	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	FreeSpotRepository freeSpotRepository;
	@Autowired
	EmployeeParkingLotRepository employeeParkingLotRepository;

	public ReleaseDto RaiseRequest(FreeSpotDto freeSpotDto) {
		ReleaseDto releaseDto = new ReleaseDto();

		Optional<Employee> optional = employeeRepository.findById(freeSpotDto.getEmployeeId());
		Employee employee = null;
		if (optional.isPresent()) {
			employee = optional.get();
		}

		else {
			throw new DataNotFoundException("employee not found");

		}
		EmployeeParkingLot employeeParkingLot = employeeParkingLotRepository.findByEmployee(employee);

		if (employeeParkingLot == null) {
			throw new DataNotFoundException("not a vip employee");

		}
		if (freeSpotDto.getStartDate().isBefore(freeSpotDto.getEndDate())) {
			if (employee.getIsVip()) {
				for (LocalDate dates = freeSpotDto.getStartDate(); dates
						.isBefore(freeSpotDto.getEndDate().plusDays(1)); dates = dates.plusDays(1)) {

					FreeSpot freeespot = new FreeSpot();
					freeespot.setEmployee(employee);
					System.out.println(employee);
					freeespot.setParkingLot(employeeParkingLot.getParkingLot());
					freeespot.setStatus("not assigned");
					freeespot.setStartDate(dates);
					freeSpotRepository.save(freeespot);
				}

				releaseDto.setMessage("raised sucessfully");
				return releaseDto;

			} else {

				throw new DataNotFoundException("employee is not vip ");
			}

		} else {
			throw new DataNotFoundException("start date must be less than end date");
		}
	}
}
