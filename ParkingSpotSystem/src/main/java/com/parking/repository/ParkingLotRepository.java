package com.parking.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parking.model.ParkingLot;

public interface ParkingLotRepository extends JpaRepository<ParkingLot, Long>{

}
