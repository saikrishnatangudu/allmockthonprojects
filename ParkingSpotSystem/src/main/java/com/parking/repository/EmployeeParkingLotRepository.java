package com.parking.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parking.model.Employee;
import com.parking.model.EmployeeParkingLot;

public interface EmployeeParkingLotRepository extends JpaRepository<EmployeeParkingLot, Long>{

	EmployeeParkingLot   findByEmployee(Employee empkoyee);
	
}
