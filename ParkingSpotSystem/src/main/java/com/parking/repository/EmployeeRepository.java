package com.parking.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parking.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
