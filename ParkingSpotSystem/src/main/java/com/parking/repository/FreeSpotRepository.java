package com.parking.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parking.model.FreeSpot;

public interface FreeSpotRepository extends JpaRepository<FreeSpot, Long>{

	List<FreeSpot> findByStartDateGreaterThanEqualAndStatus(LocalDate date, String string);

}
