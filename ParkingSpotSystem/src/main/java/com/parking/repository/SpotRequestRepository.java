package com.parking.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parking.model.Employee;
import com.parking.model.SpotRequest;

public interface SpotRequestRepository extends JpaRepository<SpotRequest, Long>{

	List<SpotRequest> findByStartDateGreaterThanEqualAndStatus(LocalDate date, String string);
	List<SpotRequest>  findByEmployee(Employee employee);
	

}
