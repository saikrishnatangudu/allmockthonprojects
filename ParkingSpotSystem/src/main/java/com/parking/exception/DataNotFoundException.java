package com.parking.exception;

public class DataNotFoundException extends RuntimeException {
	public DataNotFoundException(String string) {
		super(string);
	}

}
