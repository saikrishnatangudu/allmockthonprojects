package com.learn.Ecommerce.dto;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.model.ProductReview;

public class ProductDto {

	private int id;
	 private double rating;
		private String feedBack;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public double getRating() {
			return rating;
		}
		public void setRating(double rating) {
			this.rating = rating;
		}
		public String getFeedBack() {
			return feedBack;
		}
		public void setFeedBack(String feedBack) {
			this.feedBack = feedBack;
		}
		
}
