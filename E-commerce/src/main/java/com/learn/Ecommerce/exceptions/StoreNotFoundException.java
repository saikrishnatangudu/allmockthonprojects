package com.learn.Ecommerce.exceptions;

public class StoreNotFoundException extends Exception {

	public StoreNotFoundException(String string) {
		super(string);
	}
}
