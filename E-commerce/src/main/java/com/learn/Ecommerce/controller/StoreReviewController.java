package com.learn.Ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;    
import com.learn.Ecommerce.dto.ResponseDto;
import com.learn.Ecommerce.dto.StoreReviewDto;
import com.learn.Ecommerce.exceptions.StoreNotFoundException;

import com.learn.Ecommerce.service.StoreReviewService;

@RestController
public class StoreReviewController {
@Autowired
StoreReviewService storeReviewService;

@PostMapping("storeReview")
public ResponseEntity<ResponseDto> createReview(@RequestBody StoreReviewDto storeReviewDto) throws StoreNotFoundException  {
	
	ResponseDto message=storeReviewService.createStoreReview(storeReviewDto);
	
	return new ResponseEntity<>(message, HttpStatus.OK); 
}
}
