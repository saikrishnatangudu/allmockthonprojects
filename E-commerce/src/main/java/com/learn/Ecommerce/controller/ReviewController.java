package com.learn.Ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.learn.Ecommerce.dto.ProductDto;
import com.learn.Ecommerce.dto.ResponseDto;
import com.learn.Ecommerce.service.ReviewServiceImpl;

@RestController
public class ReviewController {

	@Autowired
	ReviewServiceImpl reviewService;
	
	@PostMapping(value="/productreview")
	public ResponseEntity<ResponseDto> review(@RequestBody ProductDto productDto) {
		ResponseDto responseDto = reviewService.review(productDto);
		return new ResponseEntity<>(responseDto,HttpStatus.OK);
	}
	
}