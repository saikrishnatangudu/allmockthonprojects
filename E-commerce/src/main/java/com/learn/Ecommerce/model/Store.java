package com.learn.Ecommerce.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table
public class Store {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer storeId;
	private Double rating;
	private String feedBack;
	
@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	private Product product;

	@OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	private List<StoreReview> StoreReview;

	public String getFeedBack() {
		return feedBack;
	}

	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	private String storeName;
	

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public List<StoreReview> getStoreReview() {
		return StoreReview;
	}

	public void setStoreReview(List<StoreReview> storeReview) {
		StoreReview = storeReview;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "Store [storeId=" + storeId + ", rating=" + rating + ", feedBack=" + feedBack + ", product=" + product
				+ ", StoreReview=" + StoreReview + ", storeName=" + storeName + "]";
	}

}
