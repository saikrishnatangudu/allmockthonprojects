package com.learn.Ecommerce.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table
public class ProductReview {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer productReviewId;
    private Double rating;
	private String feedBack;
	@JsonIgnore
	@ManyToOne( cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	private Product product;
	public Integer getProductReviewId() {
		return productReviewId;
	}
	public void setProductReviewId(Integer productReviewId) {
		this.productReviewId = productReviewId;
	}
	public Double getRating() {
		return rating;
	}
	public void setRating(Double rating) {
		this.rating = rating;
	}
	public String getFeedBack() {
		return feedBack;
	}
	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	

}
