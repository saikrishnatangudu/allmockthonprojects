package com.learn.Ecommerce.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private Double price;
	private double rating;
	

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	@ManyToOne(cascade = CascadeType.MERGE)
	private Category category;
	
	@OneToMany(mappedBy= "product", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	private List<Store> storeList;
	
	@JsonIgnore
	@OneToMany(mappedBy= "product", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	private List<ProductReview> productReviewList;
	
	public List<Store> getStoreList() {
		return storeList;
	}

	public void setStoreList(List<Store> storeList) {
		this.storeList = storeList;
	}

	
	
	public List<ProductReview> getProductReviewList() {
		return productReviewList;
	}

	public void setProductReviewList(List<ProductReview> productReviewList) {
		this.productReviewList = productReviewList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price + ", category=" + category + ", storeList="
				+ storeList + ", productReviewList=" + productReviewList + "]";
	}
	
	

}
