package com.learn.Ecommerce.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.Ecommerce.exceptions.ProductNameNotFoundException;
import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.model.ProductReview;
import com.learn.Ecommerce.model.Store;
import com.learn.Ecommerce.model.StoreReview;
import com.learn.Ecommerce.repository.ProductRepository;
import com.learn.Ecommerce.repository.ProductReviewRepository;

@Service
public class ProductService {
	@Autowired

	ProductRepository productRepository;

	@Autowired
	ProductReviewRepository productReviewRepository;

	public List<Product> viewProductByName(String productName) throws ProductNameNotFoundException {

		List<Product> products = productRepository.findByNameLike("%" + productName + "%");

		if (products.isEmpty()) {

			throw new ProductNameNotFoundException(productName);

		} else {

			List<List<Store>> store = products.stream().map(m -> m.getStoreList()).collect(Collectors.toList());

			return products;
		}

	}

	public Product getProduct(String productName) {

		Product product = productRepository.findByName(productName);

		List<ProductReview> productReview = product.getProductReviewList();
		List<ProductReview> productReview1 = productReview.stream()
				.sorted(Comparator.comparing(ProductReview::getRating).reversed()).collect(Collectors.toList());
		product.setProductReviewList(productReview1);

		List<Double> d = productReview1.stream().map(x -> x.getRating()).collect(Collectors.toList());
		OptionalDouble avg = d.stream().mapToDouble(Double::doubleValue).average();
		product.setRating((avg.getAsDouble()));
		productRepository.save(product);
		List<Store> store = product.getStoreList();
		List<Store> store1 = store.stream().sorted(Comparator.comparing(Store::getRating).reversed())
				.collect(Collectors.toList());
		product.setStoreList(store1);

		return product;
	}

	public void saveproduct(Product product) {
		productRepository.save(product);
	}

	public void saveproductReview(ProductReview product) {

		productReviewRepository.save(product);
	}

}
