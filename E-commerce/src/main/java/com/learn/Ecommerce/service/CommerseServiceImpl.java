	package com.learn.Ecommerce.service;

import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.learn.Ecommerce.dto.OrderDtoResponse;
import com.learn.Ecommerce.exceptions.TransactionFailedException;
import com.learn.Ecommerce.exceptions.UserNotFoundException;
import com.learn.Ecommerce.model.Order;
import com.learn.Ecommerce.model.User;
import com.learn.Ecommerce.repository.OrderRepository;





@Service
public class CommerseServiceImpl  {

	@Autowired
	OrderRepository orderRepo;
	@Autowired
	com.learn.Ecommerce.repository.UserRepository userRepository;
	@Autowired
	RestTemplate restTemplate;

	@Bean
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}


	public OrderDtoResponse buyProject(Order order) throws UserNotFoundException {

		double cash = this.sum(order);
	User user=	userRepository.findById(order.getUser().getUserId()).orElseThrow(()->new UserNotFoundException("user not found"));
		
		order.setUser(user);

		ResponseEntity<String> response = this.transction(order, cash);
		if (response.getStatusCode() == HttpStatus.OK) {

			order = orderRepo.save(order);
			OrderDtoResponse orderDtoResponse = new OrderDtoResponse();
			BeanUtils.copyProperties(order, orderDtoResponse);
			orderDtoResponse.setAccountNumber((int) order.getAccountNumber());
			orderDtoResponse.setTotalOrderAmmount(cash);
			orderDtoResponse.setOrderId(order.getId());
			return orderDtoResponse;

		} else {
			throw new TransactionFailedException();
		}

	}

	public ResponseEntity<String> transction(Order order, double sum) {
		String uri = "http://localhost:8081/transaction";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("accountNumber", order.getAccountNumber());
		request.put("amount", sum);
		request.put("banificiaryAccountNo", 6666);

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

		System.out.println(response);
		return response;
	}

	public double sum(Order order) {

		List<Double> d = order.getProductList().stream().map(x -> x.getPrice()).collect(Collectors.toList());
		double sum = d.stream().mapToDouble(Double::doubleValue).sum();
		return sum;

	}

	/*
	 * @GetMapping("/product") public ResponseEntity<Product>
	 * getProduct(@RequestParam String productName) {
	 * 
	 * Product product = productService.getProduct(productName);
	 * 
	 * return new ResponseEntity<>(product, HttpStatus.OK);
	 * 
	 * }
	 */

}

