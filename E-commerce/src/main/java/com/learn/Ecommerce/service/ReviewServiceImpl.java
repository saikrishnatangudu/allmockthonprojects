package com.learn.Ecommerce.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.Ecommerce.dto.ProductDto;
import com.learn.Ecommerce.dto.ResponseDto;
import com.learn.Ecommerce.exceptions.ProductNotFoundException;
import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.model.ProductReview;

import com.learn.Ecommerce.repository.ProductRepository;
import com.learn.Ecommerce.repository.ReviewRepository;
import com.learn.Ecommerce.repository.UserRepository;

@Service
public class ReviewServiceImpl {

	@Autowired
	ReviewRepository reviewRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	ProductRepository productRepository;


	public ResponseDto review(ProductDto productDto) throws ProductNotFoundException {
		Optional<Product> optional = productRepository.findById(productDto.getId());
		Product product = null;
		if (optional.isPresent()) {
			product = optional.get();
		} else {
			throw new ProductNotFoundException("product not found");
		}
		ProductReview productReview = new ProductReview();
		productReview.setFeedBack(productDto.getFeedBack());
		productReview.setProduct(product);
		productReview.setRating(productDto.getRating());
		ProductReview productreview;
		reviewRepository.save(productReview);
		ResponseDto dto = new ResponseDto();
		dto.setMessage("rating given sucessfully");
		return dto;
	}

}