package com.learn.Ecommerce.service;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.Ecommerce.dto.ResponseDto;
import com.learn.Ecommerce.dto.StoreReviewDto;
import com.learn.Ecommerce.exceptions.StoreNotFoundException;
import com.learn.Ecommerce.exceptions.UserNotFoundException;
import com.learn.Ecommerce.model.Store;
import com.learn.Ecommerce.model.StoreReview;
import com.learn.Ecommerce.model.User;
import com.learn.Ecommerce.repository.StoreRepository;
import com.learn.Ecommerce.repository.StoreReviewRepository;
import com.learn.Ecommerce.repository.UserRepository;


@Service
public class StoreReviewService {
@Autowired
StoreReviewRepository storeReviewRepository;
@Autowired
StoreRepository storeRepository;

ResponseDto responseDto = new ResponseDto();
StoreReview storeReview = new StoreReview();

public ResponseDto createStoreReview(StoreReviewDto storeReviewDto) throws StoreNotFoundException {
	
		Store store=storeRepository.findById(storeReviewDto.getStoreId()).orElseThrow(()->new StoreNotFoundException("store Id not found"));
		StoreReview storeReview=new StoreReview();
		storeReview.setFeedBack(storeReviewDto.getFeedBack());
		storeReview.setRating(storeReviewDto.getRating());
		storeReview.setStore(store);
		
		List<Double> d =store.getStoreReview().stream().map(x->x.getRating()).collect(Collectors.toList());
		System.out.println(d);
	OptionalDouble	avg= d.stream().mapToDouble(Double::doubleValue).average();
		System.out.println(avg+"---------------------------------------------------");
		store.setRating(avg.getAsDouble());
		storeRepository.save(store);
		storeReviewRepository.save(storeReview);
		responseDto.setMessage("store review successfully given");
		return responseDto;
	
}

}
