package com.learn.Ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.Ecommerce.model.StoreReview;


public interface StoreReviewRepository extends JpaRepository<StoreReview, Integer> {

}
