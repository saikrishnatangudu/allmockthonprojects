package com.learn.Ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.Ecommerce.model.ProductReview;

public interface ProductReviewRepository extends JpaRepository<ProductReview, Integer>{

}
