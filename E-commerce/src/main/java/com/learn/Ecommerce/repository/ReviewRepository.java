package com.learn.Ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.model.ProductReview;

@Repository
public interface ReviewRepository extends JpaRepository<ProductReview, Integer>{

	public Product findByProduct(Product product);

	
}
