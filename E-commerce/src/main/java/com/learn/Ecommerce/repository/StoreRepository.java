package com.learn.Ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.Ecommerce.model.Store;
import com.learn.Ecommerce.model.StoreReview;


public interface StoreRepository extends JpaRepository<Store, Integer> {
	
	

}
