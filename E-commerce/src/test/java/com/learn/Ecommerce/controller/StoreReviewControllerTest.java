package com.learn.Ecommerce.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.learn.Ecommerce.dto.OrderDtoResponse;
import com.learn.Ecommerce.dto.ResponseDto;
import com.learn.Ecommerce.dto.StoreReviewDto;
import com.learn.Ecommerce.exceptions.StoreNotFoundException;
import com.learn.Ecommerce.exceptions.UserNotFoundException;
import com.learn.Ecommerce.model.Order;
import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.model.StoreReview;
import com.learn.Ecommerce.service.CommerseServiceImpl;
import com.learn.Ecommerce.service.ProductService;
import com.learn.Ecommerce.service.StoreReviewService;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class StoreReviewControllerTest {
	@InjectMocks
	StoreReviewController storeReviewController;
	@InjectMocks
	OrderController orderController;
	@Mock
	CommerseServiceImpl orderService;
	@Mock
	StoreReviewService storeReviewService;
	@Mock
	ProductService productService;
	@InjectMocks
	ProductController controller;
	MockMvc mockMvc;
	private OngoingStubbing<ResponseDto> thenReturn;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(storeReviewController).build();
	}

	@Test
	public void testcreateReviewForPositive() throws StoreNotFoundException {
		List<StoreReview> reviews = new ArrayList<StoreReview>();
		StoreReview storeReview = new StoreReview();
		storeReview.setRating((double) 4);
		storeReview.setFeedBack("good");
		reviews.add(storeReview);
		StoreReviewDto srd = new StoreReviewDto();
		ResponseDto Rd = new ResponseDto();
		Mockito.when(storeReviewService.createStoreReview(srd)).thenReturn(Rd);
		ResponseEntity<ResponseDto> rs = storeReviewController.createReview(srd);
		Assert.assertEquals(HttpStatus.OK, rs.getStatusCode());
	}

	/*
	 * @Test public void testOrder() throws UserNotFoundException { ResponseDto
	 * orderResponseDto = new ResponseDto(); Order order = new Order();
	 * OrderDtoResponse dtoResponse = new OrderDtoResponse();
	 * Mockito.when(orderService.buyProject(order)).thenReturn(dtoResponse);
	 * 
	 * orderResponseDto.setMessage("Amount transfered successfully");
	 * ResponseEntity<ResponseDto> rd = orderController.order(order);
	 * Assert.assertEquals(HttpStatus.OK, rd.getStatusCode()); }
	 */

	@Test
	public void testGetProduct() {
		Product product = new Product();

		Mockito.when(productService.getProduct("sk")).thenReturn(product);
		ResponseEntity<Product> p = controller.getProduct("sk");
		Assert.assertEquals(HttpStatus.OK, p.getStatusCode());

	}

}
