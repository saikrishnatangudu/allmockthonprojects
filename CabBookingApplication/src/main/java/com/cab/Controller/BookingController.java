package com.cab.Controller;

import java.util.Date;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cab.Dto.BookingDto;
import com.cab.Dto.BookingResponseDto;
import com.cab.Dto.CancelBookingDto;
import com.cab.Dto.CancelResponse;
import com.cab.Dto.cabResponseDto;
import com.cab.Model.Booking;
import com.cab.Service.BookingService;


/**
 * 
 * @author saikrishnaTangudu
 * version 1.0
 *  
 */

@RestController
@RequestMapping("/cabs")
public class BookingController {
	
	Logger logger = LoggerFactory.getLogger(BookingController.class);
	
	@Autowired
	BookingService bookingService;
/**
 * 
 * @param bookingDto
 * @return BookingResponseDto
 */
	@PostMapping("booking")
	public ResponseEntity<BookingResponseDto> makeBooking(@RequestBody BookingDto bookingDto) {
		logger.info("<<------------In MakeBooking Method------------>>");
		logger.warn("<<------------In MakeBooking Method------------>>");
		logger.debug("<<------------In MakeBooking Method------------>>");
		BookingResponseDto booking = bookingService.makeBooking(bookingDto);

		return new ResponseEntity<>(booking, HttpStatus.OK);

	}
	
	/**
	 * 
	 * @param cancelBookingDto
	 * @return CancelResponse
	 */

	@PostMapping("cancelBooking")
	public ResponseEntity<CancelResponse> cancelBooking(@RequestBody CancelBookingDto cancelBookingDto) {
		logger.info("<<------------In cancelBooking Method------------>>");
		CancelResponse booking = bookingService.cancelBooking(cancelBookingDto);

		return new ResponseEntity<>(booking, HttpStatus.OK);

	}
	/**
	 * 
	 * @param historyDto
	 * @param userId
	 * @return List<Booking>
	 */

	@GetMapping("Booking")
	public ResponseEntity<List<Booking>> getBooking(@RequestParam String historyDto, @RequestParam Long userId) {
		logger.info("<<------------In getBooking Method------------>>");
		List<Booking> booking = bookingService.getBooking(historyDto, userId);

		return new ResponseEntity<>(booking, HttpStatus.OK);

	}
	
	/**
	 * 
	 * @param timestamp
	 * @return List<cabResponseDto>
	 */

	@PostMapping("/cabs")
	public ResponseEntity<List<cabResponseDto>> getAvaliableCabs(@RequestParam @DateTimeFormat(pattern = "dd.MM.yyyy") Date timestamp) {
		logger.info("<<------------In getAvaliableCabs Method------------>>");
		List<cabResponseDto> cabs=	 bookingService.getCabs(timestamp);
		 return new ResponseEntity<>(cabs, HttpStatus.OK);
	}

	/*
	 * @PostMapping("/cab") public Cab saveCab(@RequestBody Cab cab) { return
	 * bookingService.saveCab(cab); }
	 */

}
