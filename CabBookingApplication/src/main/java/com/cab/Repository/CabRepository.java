package com.cab.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cab.Model.Cab;

public interface CabRepository extends JpaRepository<Cab, Long> {

	List<Cab> findByTimeStampGreaterThan(Date timeStamp);

	List<Cab> findAllByTimeStampBetween(LocalDateTime startLocalDateTime, LocalDateTime endLocalDateTime);

}
