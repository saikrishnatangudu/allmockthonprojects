package com.cab.Service;

import com.cab.Dto.UserDto;
import com.cab.Dto.UserRegistrationDto;

public interface UserService {

	UserDto createUser(UserRegistrationDto userRegistrationDto);

}
