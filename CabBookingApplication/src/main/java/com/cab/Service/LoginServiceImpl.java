package com.cab.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cab.Dto.ResponseMessageDto;
import com.cab.Dto.UserDto;
import com.cab.Exceptions.UserNotfoundException;
import com.cab.Model.User;
import com.cab.Repository.UserRepository;

@Service
public class LoginServiceImpl implements LoginService {
	@Autowired
	UserRepository loginRepository;

	@Override
	public ResponseMessageDto login(UserDto userDto) {

		User login = loginRepository.findByEmailAndPassword(userDto.getEmail(), userDto.getPassword());
		if (login==null) {
			throw new UserNotfoundException("User doesnot exists");
		}
		ResponseMessageDto responseMessageDto = new ResponseMessageDto();
		responseMessageDto.setMessage("user Logged in Sucessfully");
		
		return responseMessageDto;
		
	}
		
	}
	


