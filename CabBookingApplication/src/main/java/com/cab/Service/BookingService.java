package com.cab.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cab.Controller.BookingController;
import com.cab.Dto.BookingDto;
import com.cab.Dto.BookingResponseDto;

import com.cab.Dto.CancelBookingDto;
import com.cab.Dto.CancelResponse;
import com.cab.Dto.HistoryDto;
import com.cab.Dto.cabResponseDto;
import com.cab.Exceptions.BookingNotFoundException;
import com.cab.Model.Booking;
import com.cab.Model.Cab;
import com.cab.Model.User;
import com.cab.Repository.BookingRepository;
import com.cab.Repository.CabRepository;
import com.cab.Repository.UserRepository;

/**
 * 
 * @author saikrishna
 * @version 1.0
 */
@Service
public class BookingService {

	Logger logger = LoggerFactory.getLogger(BookingService.class);

	@Autowired
	UserRepository userRepository;
	@Autowired
	CabRepository cabRepository;
	@Autowired
	BookingRepository bookingRepository;

	/**
	 * 
	 * @param bookingDto
	 * @return BookingResponseDto
	 */

	public BookingResponseDto makeBooking(BookingDto bookingDto) {
		Booking booking = new Booking();
		logger.info("<-------------makeBooking service------------->");
		Optional<Cab> optional = cabRepository.findById(bookingDto.getCabId());
		Cab cab = null;
		if (optional.isPresent()) {
			cab = optional.get();
		} else {
			logger.warn("cab not found");
			throw new BookingNotFoundException("cab not found");
		}

		Optional<User> option = userRepository.findById(bookingDto.getUserId());
		User user = null;
		if (option.isPresent()) {
			user = option.get();
		} else {
			logger.warn("user not found");
			throw new BookingNotFoundException("user not found");
		}
		if (cab.isCabStatus()) {
			logger.warn("cab is not avilable");
			throw new BookingNotFoundException("cab not available for booking");
		}

		cabResponseDto cabResponseDto = new cabResponseDto();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

		LocalDateTime date = LocalDateTime.now();
		LocalDateTime date2 = cab.getTimeStamp();
		int hours = cab.getTimeStamp().getHour() - date.getHour();

		booking.setBookingTime(new Date());
		System.out.println(hours > 2);
		System.out.println(date.compareTo(date2) < 0);
		if (hours > 2 || date.compareTo(date2) < 0) {

			cab.setCabStatus(true);
			cabRepository.save(cab);

			booking.setCab(cab);

			booking.setUser(user);

			booking.setBookingStatus("booked");

			String dateString = format.format(cab.getTimeStamp());
			cabResponseDto.setAvilableAt(dateString);
			BeanUtils.copyProperties(cab, cabResponseDto);
			bookingRepository.save(booking);
			BookingResponseDto bookingResponseDto = new BookingResponseDto();
			bookingResponseDto.setEmail(user.getEmail());
			bookingResponseDto.setUserName(user.getUserName());
			bookingResponseDto.setCabResponseDto(cabResponseDto);
			return bookingResponseDto;
		} else {

			throw new BookingNotFoundException("you cannot book the cab before 2 hrs");
		}

	}

	/**
	 * 
	 * @param cancelBookingDto
	 * @return CancelResponse
	 */

	public CancelResponse cancelBooking(CancelBookingDto cancelBookingDto) {
		logger.info("<-------------cancelBooking service------------->");
		Optional<User> option = userRepository.findById(cancelBookingDto.getUserId());
		User user = null;
		if (option.isPresent()) {
			user = option.get();
		} else {
			logger.warn("user is not avilable");
			throw new BookingNotFoundException("user not found");
		}

		Booking booking = bookingRepository.findByUserAndBookingId(user, cancelBookingDto.getBookingId());
		if (booking == null) {
			logger.warn("Booking is not avilable");
			throw new BookingNotFoundException("Booking not found");
		}

		Optional<Cab> optional = cabRepository.findById(booking.getCab().getCabId());
		Cab cab = null;
		if (optional.isPresent()) {
			cab = optional.get();
		} else {
			logger.warn("cab is not found");
			throw new BookingNotFoundException("cab not found");
		}
		LocalDateTime date = LocalDateTime.now();
		LocalDateTime date2 = cab.getTimeStamp();
		int hours = cab.getTimeStamp().getHour() - date.getHour();
		booking.setBookingTime(new Date());

		if (date.compareTo(date2) < 0) {
			if (hours < 1 || date.compareTo(date2) < 0) {

				cab.setCabStatus(false);
				cabRepository.save(cab);
				booking.setCab(cab);
				booking.setUser(user);
				booking.setBookingStatus("cancelled");
				bookingRepository.save(booking);
				CancelResponse cancelResponse = new CancelResponse();
				cancelResponse.setBookingId(booking.getBookingId());
				cancelResponse.setMessage("Booking cancelled sucessfully");
				return cancelResponse;
			} else {
				logger.warn("you cannot cancel the cab before 1 hr");
				throw new BookingNotFoundException("you cannot cancel the cab before 1 hr");
			}
		} else {
			logger.warn("you cannot cancel the cab before dates");
			throw new BookingNotFoundException("you cannot cancel the cab before dates");
		}

	}

	/**
	 * 
	 * @param historyDto
	 * @return List<Booking>
	 */
	public List<Booking> getBooking1(HistoryDto historyDto) {
		Optional<User> option = userRepository.findById(historyDto.getUserId());
		User user = null;
		if (option.isPresent()) {
			user = option.get();
		} else {
			logger.warn("user is not avilable");
			throw new BookingNotFoundException("user not found");
		}
		List<Booking> bookings = bookingRepository.findByUserAndBookingTimeBetween(user, historyDto.getFromDate(),
				historyDto.getToDate());
		if (bookings.isEmpty()) {
			logger.warn("bookings for particular date are not there");
			throw new BookingNotFoundException("bookings for particular date are not there");
		}

		return bookings;
	}

	/**
	 * 
	 * @param timestamp
	 * @return List<cabResponseDto>
	 * @throws BookingNotFoundException
	 */

	public List<cabResponseDto> getCabs(Date timestamp) throws BookingNotFoundException {

		LocalDate localDate = timestamp.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		List<Cab> cabs;
		if (LocalDate.now().isEqual(localDate)) {
			LocalDateTime startLocalDateTime = LocalDateTime.now();
			LocalDateTime endLocalDateTime = localDate.atTime(23, 59);

			cabs = cabRepository.findAllByTimeStampBetween(startLocalDateTime, endLocalDateTime);

		} else {

			LocalDateTime startLocalDateTime = localDate.atStartOfDay();
			LocalDateTime endLocalDateTime = localDate.atTime(23, 59);

			cabs = cabRepository.findAllByTimeStampBetween(startLocalDateTime, endLocalDateTime);

		}

		if (cabs.isEmpty()) {

			throw new BookingNotFoundException("no cabs found");
		}
		List<cabResponseDto> cbrd = new ArrayList<>();
		for (Cab cab : cabs) {
			cabResponseDto cabResponseDto = new cabResponseDto();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			String dateString = formatter.format(cab.getTimeStamp());
			cabResponseDto.setAvilableAt(dateString);
			BeanUtils.copyProperties(cab, cabResponseDto);
			cbrd.add(cabResponseDto);

		}

		return cbrd;
	}

	/**
	 * 
	 * @param historyDto
	 * @param userId
	 * @return List<Booking>
	 */
	public List<Booking> getBooking(String historyDto, Long userId) {

		List<Booking> bookings = bookingRepository.findByMatchMonthAndMathDay(historyDto, userId);
		if (bookings.isEmpty()) {
			logger.warn("no bookings found");
			throw new BookingNotFoundException("no bookings found");
		}

		return bookings;

	}
	/*	*//**
			 * 
			 * @param cab
			 * @return Cab
			 *//*
				 * 
				 * public Cab saveCab(Cab cab) {
				 * 
				 * return cabRepository.save(cab); }
				 */
}
