package com.cab.Service;

import com.cab.Dto.ResponseMessageDto;
import com.cab.Dto.UserDto;
import com.cab.Model.User;

public interface LoginService {

	ResponseMessageDto login(UserDto userDto);

}
