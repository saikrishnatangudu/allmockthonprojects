package com.cab.Exceptions;

public class UserNotfoundException extends RuntimeException {

	public UserNotfoundException(String message) {
		super(message);
	}
	
}