package com.cab.Dto;

import java.util.Date;

public class SearchDto {
	
	 
	 private Date timeStamp;

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

}
