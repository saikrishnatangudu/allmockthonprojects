package com.example.hungerbox.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.hungerbox.exceptions.EmployeeNotFoundException;
import com.example.hungerbox.model.Employee;
import com.example.hungerbox.model.Order;
import com.example.hungerbox.model.OrderItemList;
import com.example.hungerbox.repository.OrderItemListRepository;
import com.example.hungerbox.repository.OrderRepository;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OrderDetailsServiceImplTest {


	@InjectMocks
	OrderDetailsServiceImplTest orderDetailsServiceImpl;

	@Mock
	OrderRepository orderItemRepository;
	
	@Test
	public void TestorderListForPositive() throws EmployeeNotFoundException {
		List<Order> siva= new ArrayList();
		OrderItemList orderItemList = new OrderItemList();
		orderItemList.setId(1L);
		orderItemList.setQuantity(2);
	    Employee employee = new Employee();
	    employee.setEmployeeId(1L);
	    employee.setEmployeeName("siva");
	    Mockito.when(orderItemRepository.findOrderByEmployee(employee)).thenReturn(siva);
	    Assert.assertNotNull(employee);
	   
	}
	@Test
	public void TestorderListForNagative() throws EmployeeNotFoundException {
		List<Order> siva= new ArrayList();
		OrderItemList orderItemList = new OrderItemList();
		orderItemList.setId(-1L);
		orderItemList.setQuantity(2);
	    Employee employee = new Employee();
	    employee.setEmployeeId(1L);
	    employee.setEmployeeName("siva");
	    Mockito.when(orderItemRepository.findOrderByEmployee(employee)).thenReturn(siva);
	    Assert.assertNotNull(employee);
	   
	}
	@Test(expected = NullPointerException.class)
	public void TestorderListForNegitive() throws EmployeeNotFoundException {
		List<Order> siva= new ArrayList();
		OrderItemList orderItemList = new OrderItemList();
		orderItemList.setId(-1L);
		Employee employee = new Employee();
		employee.setEmployeeId(1L);
		employee.setEmployeeName("siva");
		Mockito.when(orderItemRepository.findOrderByEmployee(employee)).thenReturn(siva);
		//EmployeeOrder kumar = orderDetailsServiceImpl.orderList((Long) null);
		
	}
	@Test(expected = NullPointerException.class)
	public void TestorderListForPositive1() throws EmployeeNotFoundException {
		List<Order> siva= new ArrayList();
		OrderItemList orderItemList = new OrderItemList();
		orderItemList.setId(1L);
		Employee employee = new Employee();
		employee.setEmployeeId(1L);
		employee.setEmployeeName("siva");
		Mockito.when(orderItemRepository.findOrderByEmployee(employee)).thenReturn(siva);
		//EmployeeOrder kumar = orderDetailsServiceImpl.orderList((Long) null);
		
	}
}
