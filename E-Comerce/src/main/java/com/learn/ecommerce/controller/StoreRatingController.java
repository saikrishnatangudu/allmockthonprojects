package com.learn.ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.learn.ecommerce.dto.StoreReviewDto;
import com.learn.ecommerce.model.Store_review;
import com.learn.ecommerce.service.ProductService;

@RestController
public class StoreRatingController {
	@Autowired
	ProductService productService;

	@PostMapping("/storeReview")
	public ResponseEntity<Store_review> storeReview(@RequestBody StoreReviewDto storeReviewDto) {

		Store_review product = productService.storeRating(storeReviewDto);

		return new ResponseEntity<>(product, HttpStatus.OK);

	}
}
