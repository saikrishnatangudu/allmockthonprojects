package com.learn.ecommerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learn.ecommerce.dto.ProductDto;
import com.learn.ecommerce.dto.ProductRatingDto;
import com.learn.ecommerce.dto.StoreReviewDto;
import com.learn.ecommerce.model.Product;
import com.learn.ecommerce.model.Product_rating;
import com.learn.ecommerce.model.Store_Product;
import com.learn.ecommerce.model.Store_review;
import com.learn.ecommerce.service.ProductService;

@RestController
public class StoreproductController {

	@Autowired
	ProductService productService;

	@GetMapping("/product")
	public ResponseEntity<ProductDto> getProduct(@RequestParam String productName) {

		ProductDto product = productService.viewProductByName(productName);

		return new ResponseEntity<>(product, HttpStatus.OK);

	}

}
