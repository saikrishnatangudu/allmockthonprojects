package com.learn.ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.learn.ecommerce.dto.ProductRatingDto;
import com.learn.ecommerce.model.Product_rating;
import com.learn.ecommerce.service.ProductService;

@RestController
public class ProductRatingController {

	@Autowired
	ProductService productService;

	@PostMapping("/productReview")
	public ResponseEntity<Product_rating> productReview(@RequestBody ProductRatingDto productRatingDto) {

		Product_rating product = productService.productRating(productRatingDto);

		return new ResponseEntity<>(product, HttpStatus.OK);

	}
}
