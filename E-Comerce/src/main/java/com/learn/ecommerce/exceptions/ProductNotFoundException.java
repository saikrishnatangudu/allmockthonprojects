package com.learn.ecommerce.exceptions;

public class ProductNotFoundException extends RuntimeException {

	public ProductNotFoundException(String string) {
		super(string);
	}

}
