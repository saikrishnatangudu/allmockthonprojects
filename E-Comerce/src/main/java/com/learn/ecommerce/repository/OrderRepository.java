package com.learn.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.ecommerce.model.Product;
import com.learn.ecommerce.model.Store;
import com.learn.ecommerce.model.User;
import com.learn.ecommerce.model.UserOrder;

public interface OrderRepository extends JpaRepository<UserOrder, Long>{

	UserOrder findOrderByUserAndProduct(User user, Product product);

	List<UserOrder> findOrderByUserAndStore(User user, Store store);

}
