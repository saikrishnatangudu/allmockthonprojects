package com.learn.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.learn.ecommerce.model.Store;
import com.learn.ecommerce.model.Store_review;
@Repository
public interface Store_reviewRepository extends JpaRepository<Store_review,Long> {
List<Store_review> findStore_reviewsByStore(Store store);
}
