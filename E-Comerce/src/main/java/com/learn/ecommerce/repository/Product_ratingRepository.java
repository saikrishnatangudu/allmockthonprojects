package com.learn.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.ecommerce.model.Product;
import com.learn.ecommerce.model.Product_rating;

public interface Product_ratingRepository extends JpaRepository<Product_rating,Long>{
 List<Product_rating> findProduct_ratingByProduct(Product product);
}
