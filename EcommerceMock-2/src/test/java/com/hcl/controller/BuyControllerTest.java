package com.hcl.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.dto.OrderDtoResponse;
import com.hcl.model.Order;

import com.hcl.service.CommerseService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BuyControllerTest {

	@InjectMocks
	EcommerceController ecommerceController;

	@Mock
	CommerseService commerseService;

	@Test
	public void buyNegative() {
		
		Order order = new Order();
		OrderDtoResponse orderDtoResponse = new OrderDtoResponse();
		Mockito.when(commerseService.buyProject(order)).thenReturn(orderDtoResponse);
		ResponseEntity<OrderDtoResponse> s = new ResponseEntity(HttpStatus.BAD_REQUEST);

		Assert.assertEquals(HttpStatus.BAD_REQUEST, s.getStatusCode());

	}

	@Test
	public void buyPositive() {
		
		Order order = new Order();
		OrderDtoResponse orderDtoResponse = new OrderDtoResponse();
		Mockito.when(commerseService.buyProject(order)).thenReturn(orderDtoResponse);
		ResponseEntity<OrderDtoResponse> s = new ResponseEntity(HttpStatus.OK);

		Assert.assertEquals(HttpStatus.OK, s.getStatusCode());

	}

}
