package com.hcl.trainingmanagement.model;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table
public class Training {
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	private Long trainingId;
	private String batchName;
	private LocalDate startDate;
	private LocalDate endDate;
	
	@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
	private Course course;

	public Long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Training(String batchName, LocalDate startDate, LocalDate endDate, Course course) {
		super();
		this.batchName = batchName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.course = course;
	}

	public Training() {
		super();
	}
	
	
	
	

}
