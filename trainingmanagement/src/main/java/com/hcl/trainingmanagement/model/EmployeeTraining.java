package com.hcl.trainingmanagement.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class EmployeeTraining {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	

	@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
	private Course course;

	@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
	private Employee employee;
	

	@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
	private Training training;
	
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public EmployeeTraining(Course course, Employee employee, Training training, String status) {
		super();
		this.course = course;
		this.employee = employee;
		this.training = training;
		this.status = status;
	}

	public EmployeeTraining() {
		super();
		
	}
	
	

}
