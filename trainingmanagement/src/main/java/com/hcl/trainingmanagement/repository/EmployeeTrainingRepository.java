package com.hcl.trainingmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.trainingmanagement.model.EmployeeTraining;

public interface EmployeeTrainingRepository  extends JpaRepository<EmployeeTraining, Long>{

}
