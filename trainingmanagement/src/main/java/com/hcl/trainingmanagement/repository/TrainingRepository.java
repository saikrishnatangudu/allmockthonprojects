package com.hcl.trainingmanagement.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.trainingmanagement.model.Course;
import com.hcl.trainingmanagement.model.Training;

public interface TrainingRepository extends JpaRepository<Training, Long>{

	List<Training> findByCourse(Course course);
	Training findByStartDateAndEndDate(LocalDate startDate,LocalDate endDate);
}
