package com.hcl.trainingmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.trainingmanagement.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
