package com.hcl.trainingmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.trainingmanagement.model.Training;
import com.hcl.trainingmanagement.service.SearchService;

@RestController
@RequestMapping("/trainings")
public class TrainingController {
	@Autowired
	SearchService searchService;
	
	@GetMapping("/search")
	public List<Training> getTrainingDetailsByCourseName(@RequestParam String courseName){
		
		return searchService.getTrainingsByCourseName(courseName);
		
	}

}
