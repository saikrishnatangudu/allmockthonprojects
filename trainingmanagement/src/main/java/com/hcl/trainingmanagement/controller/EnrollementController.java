package com.hcl.trainingmanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.trainingmanagement.dto.EnrollDto;
import com.hcl.trainingmanagement.dto.ResponseMessageDto;
import com.hcl.trainingmanagement.service.EnrollmentService;

@RestController
@RequestMapping("/employee")
public class EnrollementController {
	
	@Autowired
	EnrollmentService enrollmentService;
	
	@PostMapping("/enroll")
	public ResponseEntity<ResponseMessageDto> enrollInTheCourse(@Validated @RequestBody EnrollDto enrollDto){
		
		ResponseMessageDto responseMessageDto=enrollmentService.registerCourse(enrollDto);
		return new ResponseEntity<>(responseMessageDto, HttpStatus.CREATED);
	}

}
