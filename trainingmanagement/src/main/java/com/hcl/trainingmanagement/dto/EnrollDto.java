package com.hcl.trainingmanagement.dto;

import com.sun.istack.NotNull;

public class EnrollDto {
	
	@NotNull
	private long employeeId;
	
	@NotNull
	private long trainingId;

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(long trainingId) {
		this.trainingId = trainingId;
	}
	
	

}
