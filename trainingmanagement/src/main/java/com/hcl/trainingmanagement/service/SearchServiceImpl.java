package com.hcl.trainingmanagement.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.trainingmanagement.exception.CourseNotFoundException;
import com.hcl.trainingmanagement.exception.TrainingDetailsNotFoundException;
import com.hcl.trainingmanagement.model.Course;
import com.hcl.trainingmanagement.model.Training;
import com.hcl.trainingmanagement.repository.CourseRepository;
import com.hcl.trainingmanagement.repository.TrainingRepository;
@Service
public class SearchServiceImpl implements SearchService{
@Autowired
	CourseRepository courseRepository;

@Autowired
TrainingRepository trainingRepository;
	@Override
	public List<Training> getTrainingsByCourseName(String courseName) {
		
		Course course=courseRepository.findByCourseName(courseName).orElseThrow(()->new CourseNotFoundException("course not found"));
		List<Training> trainings=trainingRepository.findByCourse(course);
		if(trainings.isEmpty()) {
			throw new TrainingDetailsNotFoundException("no trainings are available");
		}
		LocalDate todayDate=LocalDate.now();
		/*
		 * trainings=trainings.stream().filter(t->(t.getStartDate().isAfter(todayDate))&
		 * &(t.getEndDate().isAfter(t.getStartDate()))).collect(Collectors.toList());
		 * if(trainings.isEmpty()) { throw new
		 * TrainingDetailsNotFoundException("no trainings are available"); }
		 */
		
		return trainings;
	}

}
