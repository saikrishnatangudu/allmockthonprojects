package com.hcl.trainingmanagement.service;

import com.hcl.trainingmanagement.dto.EnrollDto;
import com.hcl.trainingmanagement.dto.ResponseMessageDto;

public interface EnrollmentService {

	public ResponseMessageDto registerCourse(EnrollDto enrollDto);
}
