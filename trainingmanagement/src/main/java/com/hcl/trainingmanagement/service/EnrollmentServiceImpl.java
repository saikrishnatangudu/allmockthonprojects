package com.hcl.trainingmanagement.service;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.trainingmanagement.dto.EnrollDto;
import com.hcl.trainingmanagement.dto.ResponseMessageDto;
import com.hcl.trainingmanagement.exception.CourseNotFoundException;
import com.hcl.trainingmanagement.model.Employee;
import com.hcl.trainingmanagement.model.EmployeeTraining;
import com.hcl.trainingmanagement.model.Training;
import com.hcl.trainingmanagement.repository.CourseRepository;
import com.hcl.trainingmanagement.repository.EmployeeRepository;
import com.hcl.trainingmanagement.repository.EmployeeTrainingRepository;
import com.hcl.trainingmanagement.repository.TrainingRepository;

@Service
public class EnrollmentServiceImpl implements EnrollmentService{
	
	@Autowired
	TrainingRepository trainingRepository;
	
	@Autowired
	EmployeeTrainingRepository employeeTrainingRepository;
	
	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public ResponseMessageDto registerCourse(EnrollDto enrollDto) {
		
		ResponseMessageDto responseMessageDto=new ResponseMessageDto();
		
		Employee employee=employeeRepository.findById(enrollDto.getEmployeeId()).orElseThrow(()->new CourseNotFoundException("employee not found"));
		
		Training training=trainingRepository.findById(enrollDto.getTrainingId()).orElseThrow(()->new CourseNotFoundException("training not found"));
		
		EmployeeTraining employeeTraining=new EmployeeTraining();
		employeeTraining.setEmployee(employee);
		employeeTraining.setCourse(training.getCourse());
		employeeTraining.setTraining(training);
		LocalDate startDate=LocalDate.now();
		if(training.getStartDate().isAfter(startDate)) {
			employeeTraining.setStatus("enrolled");
			employeeTrainingRepository.save(employeeTraining);
			responseMessageDto.setMessage("You have enrolled successfully");
			
		}
		else {
			responseMessageDto.setMessage("please try again");
		}
		return responseMessageDto;
	}
	

}
