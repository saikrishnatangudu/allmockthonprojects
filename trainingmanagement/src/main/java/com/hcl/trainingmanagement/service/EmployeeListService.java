package com.hcl.trainingmanagement.service;

import java.util.List;

import com.hcl.trainingmanagement.model.EmployeeTraining;

public interface EmployeeListService {

	List<EmployeeTraining> getEmployeeListByCourseName(String courseName);
}
