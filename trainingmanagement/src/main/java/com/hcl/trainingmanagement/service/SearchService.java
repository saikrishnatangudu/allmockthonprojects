package com.hcl.trainingmanagement.service;

import java.util.List;

import com.hcl.trainingmanagement.model.Training;

public interface SearchService {
	
	List<Training> getTrainingsByCourseName(String courseName);

}
