package com.hcl.trainingmanagement.service;

 

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doThrow;

 

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

 

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

 

import com.hcl.trainingmanagement.exception.TrainingDetailsNotFoundException;
import com.hcl.trainingmanagement.model.Course;
import com.hcl.trainingmanagement.model.Training;
import com.hcl.trainingmanagement.repository.CourseRepository;
import com.hcl.trainingmanagement.repository.TrainingRepository;

 

@RunWith(MockitoJUnitRunner.Silent.class)
public class SearchServiceImplTest {
    
    @InjectMocks
    SearchServiceImpl searchServiceImpl;
    
    @Mock
    TrainingRepository trainingRepository;
    
    @Mock
    CourseRepository courseRepository;
    
    @Test(expected = TrainingDetailsNotFoundException.class)
    public void TestGetTrainingsByCourseNameForPositive(){
        
        Course course=new Course();
        course.setCourseId(1L);
        course.setCourseName("java");
        course.setDescription("java8features");
        
        List<Training> trainings=new ArrayList<>();
        Training training=new Training();
        training.setBatchName("batch1");
        training.setCourse(course);
        training.setStartDate(LocalDate.now());
        training.setEndDate(LocalDate.of(2020,06,23)); 
        training.setTrainingId(1L); 
        trainings.add(training);
        
        Mockito.when(courseRepository.findByCourseName(Mockito.anyString())).thenReturn(Optional.of(course));
        Mockito.when(trainingRepository.findByCourse(Mockito.any())).thenReturn(trainings);
        
        List<Training> result=searchServiceImpl.getTrainingsByCourseName(course.getCourseName());
        assertNotNull(result);
        
    }
    
    @Test(expected = TrainingDetailsNotFoundException.class)
    public void TestGetTrainingsByCourseNameForNegative(){
        
        Course course=new Course();
        course.setCourseId(1L);
        course.setCourseName("javatpoint");
        course.setDescription("java8features");
        
        List<Training> trainings=new ArrayList<>();
        Training training=new Training();
        training.setBatchName("batch1");
        training.setCourse(course);
        training.setStartDate(LocalDate.now());
        training.setEndDate(LocalDate.of(2020,06,23)); 
        training.setTrainingId(1L); 
        trainings.add(training);
        
        Mockito.when(courseRepository.findByCourseName(Mockito.anyString())).thenReturn(Optional.of(course));
        Mockito.when(trainingRepository.findByCourse(Mockito.any())).thenReturn(trainings);
        
        List<Training> result=searchServiceImpl.getTrainingsByCourseName(course.getCourseName());
        assertEquals("java",result);
        
    }
    
}