package com.hcl.trainingmanagement.controller;

 

import java.util.ArrayList;
import java.util.List;

 

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

 

import com.hcl.trainingmanagement.model.Training;
import com.hcl.trainingmanagement.service.SearchServiceImpl;

 

@RunWith(MockitoJUnitRunner.Silent.class)
public class TrainingControllerTest {
    
    @InjectMocks
    TrainingController trainingController;
    
    @Mock
    SearchServiceImpl searchServiceImpl;
    
    @Test
    public void TestGetTrainingDetailsByCourseNameForPositive() {
        List<Training> trainings=new ArrayList<>();
        Training training=new Training();
        training.setBatchName("batch1");
        training.setTrainingId(1L);
        trainings.add(training);
        
        Mockito.when(searchServiceImpl.getTrainingsByCourseName(Mockito.anyString())).thenReturn(trainings);
        
        List<Training> result=trainingController.getTrainingDetailsByCourseName(Mockito.anyString());
    }
    
    @Test
    public void TestGetTrainingDetailsByCourseNameForNegative() {
        List<Training> trainings=new ArrayList<>();
        Training training=new Training();
        training.setBatchName("batch2");
        training.setTrainingId(1L);
        trainings.add(training);
        
        Mockito.when(searchServiceImpl.getTrainingsByCourseName(Mockito.anyString())).thenReturn(trainings);
        
        List<Training> result=trainingController.getTrainingDetailsByCourseName(Mockito.anyString());
    }

 

}